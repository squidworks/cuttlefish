/*
hunks/adhoc/tpath.js

collect a path, dish slowly one-pt-at-a-time

Jake Read at the Center for Bits and Atoms with Neil Gershenfeld and Leo McElroy
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

export default function PathGap(){
  Hunkify(this)

  let storage = []
  let path = []

  // to display,
  let dom = this.document()
  let text = $('<div>').addClass('txt').get(0)
  dom.appendChild(text)
  let updatePLenDisplay = () => {
    $(text).html(`path length: ${path.length}`)
  }
  updatePLenDisplay()

  // we intake big paths,
  let inPath = this.input('reference', 'path')
  // we output positions one-at-a-time,
  let outPosn = this.output('array', 'position')
  // and output the remaining path, in case we would like to re-render ...
  let outPath = this.output('reference', 'remaining')

  // we have stop / start here, and reset
  let runner = this.state('boolean', 'running', false)
  runner.onChange = (val) => {
    runner.set(val)
  }
  let empty = this.state('boolean', 'empty', false)
  empty.onChange = (val) => {
    path = []
    updatePLenDisplay()
    storage = []
  }
  let reset = this.state('boolean', 'reset', false)
  reset.onChange = (val) => {
    path = JSON.parse(JSON.stringify(storage))
    updatePLenDisplay()
    reset.set(false)
  }


  // copies out, one at a time, fc pressure
  this.loop = () => {
    // if no path, and path available, pull
    if(inPath.io()){
      if(path.length == 0){
        // get two copies,
        path = JSON.parse(JSON.stringify(inPath.get()))
        storage = JSON.parse(JSON.stringify(path))
        updatePLenDisplay()
      } else {
        inPath.get() // clear it for our friends, if we don't want it...
      }
    }
    // if we have path and can push outputs,
    if(path.length > 0 && runner.value){
      if(!outPosn.io()){
        let op = path.shift()
        updatePLenDisplay()
        console.log('pg sends', op)
        outPosn.put(op)
        // and update the downstream ref, for rendering ...
        if(!outPath.io()){
          outPath.put(path)
        }
      }
    }
  }
}
