/*
hunks/data/logReferences.js

logs (almost) anything

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import { Hunkify, Input, Output, State } from '../hunks.js'

export default function ReferenceLogger() {
  Hunkify(this)

  let tolog = this.input('reference', 'tolog')

  let prefix = this.state('string', 'prefix', 'LOG:')
  let logToConsole = this.state('boolean', 'console', true)

  // get a piece of the DOM
  let dom = this.document()
  let text = $('<div>').addClass('txt').append('->').get(0)
  $(dom).append(text)

  // if we have dom, init happens once our dom exists
  // in the document, and is visible
  // i.e. equivalent to window.onload()
  this.init = () => {
    // not always necessary
  }

  this.loop = () => {
    // this will be called once every round turn
    // typically we check flow control first
    if (tolog.io()) {
      // an input is occupied, and the exit path is empty
      let raw = tolog.get()
      let stringRep
      if (Array.isArray(raw)) {
        stringRep = raw.join(', ')
      } else if (typeof raw === "boolean") {
        stringRep = raw.toString()
      } else {
        // let js do w/e witchcraft it chooses
        stringRep = raw
      }
      $(text).html(stringRep)
      if (logToConsole.value === true) {
        console.log(this.ind, prefix.value, raw)
      }
    }
  }
}
