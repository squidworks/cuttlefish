/*
hunks/data/accumulator.js

stashes data stream to an array

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

export default function BuildTable(){
  Hunkify(this)

  // todo: make this polymorphic
  let ipr = this.input("boolean", "reset")
  let ivx = this.input("number", "x")
  let ivy = this.input("number", "y")
  // push a reference out, for now
  let outref = this.output("reference", "accumulated")
  // reset via button as well,
  let rstrig = this.state("boolean", "reset", false)

  let reset = () => {
    arr = []
    if(!outref.io()){
      outref.put(arr)
    }
  }

  rstrig.onChange = (value) => {
    reset()
    rstrig.set(false)
  }

  this.init = () => {
    //
  }

  let arr = []

  this.loop = () => {
    if(ipr.io()){
      ipr.get()
      reset()
    }
    if(ivx.io() && ivy.io()){
      arr.push([ivx.get(), ivy.get()])
      // todo: for ref, pass always OK ? breaks rules anyways ...
      if(!outref.io()){
        outref.put(arr)
      }
    }
  }
}
