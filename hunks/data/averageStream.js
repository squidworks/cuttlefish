/*
hunks/data/exFilter.js

removes old lovers from data streams

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import { Hunkify, Input, Output, State } from '../hunks.js'

export default function AverageStream(){
  Hunkify(this)

  let inNum = this.input('number', 'vals')
  let outNum = this.output('number', 'average')
  let reset = this.state('boolean', 'reset', false)
  let count = this.state('number', 'count', 10)
  let counter = this.state('number', 'counter', 0)

  counter.onChange = (val) => {
    counter.set(counter.val) // no,
  }

  let sum = 0

  reset.onChange = (value) => {
    counter.set(0)
    sum = 0
  }

  this.loop = () => {
    if(inNum.io() && (counter.value < count.value)){
      sum += inNum.get()
      let tally = counter.value + 1
      counter.set(tally)
    }
    if(!outNum.io() && (counter.value >= count.value)){
      outNum.put(sum / count.value)
      sum = 0
      counter.set(0)
    }
  }
}
