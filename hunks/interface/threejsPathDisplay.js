/*
hunks/interface/threejs_ghost.js

takes array for point, draws history of segements in tail
a-la snake, but you can never win or loose

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

import * as THREE from '../../libs/three.module.js'
import { OrbitControls } from '../../libs/three_orbitcontrols.module.js'
// really hacking the import here... apologies, works though!
import { MeshLine, MeshLineMaterial } from '../../libs/three_meshline.module.js'

export default function ThreeGhosts() {
  Hunkify(this)

  let pathIn = this.input('reference', 'point')

  let lineWidth = this.state('number', 'line width', 2)

  let dom = this.document()

  this.init = () => {

  }

  let width = 600
  let height = 600

  let camera = new THREE.PerspectiveCamera(75, width / height, 1, 10000)
  camera.up.set(0, 0, 1)
  let scene = new THREE.Scene()

  // fires when loaded into the dom,
  this.init = () => {
    scene.background = new THREE.Color(0xc8e6e2)

    let renderer = new THREE.WebGLRenderer()
    renderer.setSize(width, height)

    dom.appendChild(renderer.domElement)
    let axesHelper = new THREE.AxesHelper(10)
    scene.add(axesHelper)

    let controls = new OrbitControls(camera, this.dom)
    controls.update()
    camera.position.set(-100, -150, 400)
    camera.lookAt(new THREE.Vector3(100, 100, 0))

    let animate = function() {
      requestAnimationFrame(animate)
      //setTimeout(animate, 500)
      controls.update()
      renderer.render(scene, camera)
    }
    // kickstart
    animate()
  }

  let last = {}

  this.loop = () => {
    if (pathIn.io()) {
      // local copy,
      let lp = JSON.parse(JSON.stringify(pathIn.get()))
      let lw = lineWidth.value
      // redraw top->bottom every time ?
      let geometry = new THREE.Geometry()
      for(let i = 0; i < lp.length; i ++){
        geometry.vertices.push(new THREE.Vector3(lp[i][0], lp[i][1], lp[i][2]))
      }
      let line = new MeshLine() // first arg is pts, 2nd is width setting fn
      line.setGeometry(geometry, function(p) { return lw })
      let material = new MeshLineMaterial({
        color: 0x000000,
        resolution: new THREE.Vector2(width, height)
      })
      let mesh = new THREE.Mesh(line.geometry, material)
      scene.remove(last)
      scene.add(mesh)
      last = mesh
    }
  }
}
