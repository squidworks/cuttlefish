/*
hunks/interface/arrowpad.js

arrowpad pressure

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

export default function JogPad() {
  Hunkify(this)

  let pass = this.output('array', 'keysdown')

  const pairs = [{
    name: 'left',
    code: 37
  }, {
    name: 'right',
    code: 39
  }, {
    name: 'up',
    code: 38
  }, {
    name: 'down',
    code: 40
  }, {
    name: 'pgup',
    code: 33
  }, {
    name: 'pgdown',
    code: 34
  }]

  for (let pair of pairs) {
    // current state,
    pair.down = false
  }

  let dom = this.document()

  let activeStatus = false;
  let activeColor = '#d981eb'
  let idleColor = '#82aef5'

  let removeKeyBindings = () => {
    document.removeEventListener('keydown', keyDownListen)
    document.removeEventListener('keyup', keyUpListen)
    // no sticky keys!
    clearAllKeys()
  }

  let setKeyBindings = () => {
    document.addEventListener('keydown', keyDownListen)
    document.addEventListener('keyup', keyUpListen)
  }

  let keyDownListen = (evt) => {
    if (evt.repeat) return
    let key = pairs.find((cand) => {
      return cand.code === evt.keyCode
    })
    if (key) {
      key.down = true
    }
  }

  let keyUpListen = (evt) => {
    let key = pairs.find((cand) => {
      return cand.code === evt.keyCode
    })
    if (key) {
      key.down = false
    }
  }

  let clearAllKeys = () => {
    for (let key of pairs) {
      key.down = false
    }
  }

  let container = $('<div>').get(0)
  let msg = $('<div>').text('~ click in to activate ~').get(0)
  $(msg).css('padding-top', '35px')
  $(container).append(msg)
  $(container).css('background-color', idleColor)
  $(container).width(285).height(185)
  $(container).css('text-align', 'center')
  $(container).css('font-size', '18px')
  $(container).css('color', 'white')
  $(dom).append(container)
  dom.addEventListener('click', (evt) => {
    if (activeStatus) {
      activeStatus = false
      $(msg).text('~ click in to activate ~')
      $(container).css('background-color', idleColor)
      removeKeyBindings()
    } else {
      activeStatus = true
      $(msg).text('~ push push push ~')
      $(container).css('background-color', activeColor)
      setKeyBindings()
    }
  })


  // puts true when transitioning to down, puts false when lifting up
  // downstream evts can do whatever they like, either track state or
  // also run safety timeouts

  // this is actually kind of a nontrivial statemachine, bc we have to
  // potentially flow-control buffer output events in the order that they
  // happened ... use pairs, setup each one as a statemachine
  this.loop = () => {
    if(!pass.io()){
      // collect states,
      let state = []
      for(let key of pairs){
        if(key.down){
          state.push(key.name)
        }
      }
      pass.put(JSON.parse(JSON.stringify(state)))
    }
  }
}
