/*
hunks/interface/number.js

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

export default function Number() {
    Hunkify(this)

    let numout = this.output('number', 'num')

    let numrep = this.state('number', 'numrep', 1000)

    // as is tradition,
    let dom = this.document()
    let contact = $('<div>').addClass('btn').append('! contact !').get(0)
    contact.addEventListener('click', (evt) => {
      if(numout.io()){
        console.warn('number stream occupied, bailing')
        return
      }
      numout.put(numrep.value)
    })
    $(dom).append(contact)

    this.loop = () => {
      //
    }
}
