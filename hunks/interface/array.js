/*
hunks/interface/array.js

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

export default function Number() {
    Hunkify(this)

    let ip = this.input('string', 'arr')
    let op = this.output('array', 'arr')
    let opv = this.state('string', 'csv', '0, 0, 0')

    // str -> arr (nums)
    let convert = (str) => {
      let arr = opv.value.split(',')
      for(let i = 0; i < arr.length; i ++){
        arr[i] = parseFloat(arr[i])
      }
      return arr
    }

    let dom = this.document()
    let contact = $('<div>').addClass('btn').append('! ship it !').get(0)
    contact.addEventListener('click', (evt) => {
      if(op.io()){
        console.warn("array attempts to push to occupied output")
      } else {
        let arr = convert(opv.value)
        op.put(arr)
      }
    })
    $(dom).append(contact)

    this.loop = () => {
      if(ip.io() && !op.io()){
        let str = ip.get()
        // upd8 the state for this, as well...
        opv.set(str)
        let arr = convert(str)
        op.put(arr)
      }
    }
}
