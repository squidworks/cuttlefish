/*
hunks/view.js

gather, maintain remote (or local) graph content
run recipes on said items

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks/hunks.js'

import {
  TSET, // typset
  MK, // manager keys,
  HK, // hunk keys,
  MSGS, // messaging
  isIntType,
  isFloatType,
  isNumType
} from '../typeset.js'

export default function View() {
  Hunkify(this)

  let verbose = false
  let msgverbose = false

  let msgsin = this.input('byteArray', 'msgs')
  let msgsout = this.output('byteArray', 'msgs')

  // our bbs
  let defs = new Array
  this.defs = defs

  // some global state for the remote interpreter:
  this.interpreterName = null
  this.interpreterVersion = null

  // and our view state, stored b/c we are storing positions - etc - in the graphs, atm
  this.open = false
  this.size = { x: 900, y: 400 }

  /*
  views have some persistent state, and we link them to one another, so this is nice to know,
  views have this shape: {
    defs: [<ptrs to defs>],           // objects, see below
    interpreterName: <string>,        // name of df world we are talking to,
    interpreterVersion: <string>,     // of the same,
    size: {w: <number>, h: <number>}, // size to render at,
    open: <boolean>                   // render / no
  }
  */

  /*
  the view maintains this list of definitions to match state of remote dataflow environments
  defs have this shape: {
    type: <string>,             // the type of the thing (from which code object did it originate)
    name: <string>,             // the name of the thing,
    ind: <number>,              // own index of the def, in this array (and the managers),
    context: <ptr to view>,     // so def.context.defs[def.ind] is circular,
    // LINK-specific:
    contains: <ptr to view>,    // ~ potentially ~ exists, if this is a link piped to a view (per upper level connectivity!)
    downstream: <ptr to link>,  // ~ potentially ~ exists, meaning this bridges to that link, and that link is 'down' one level
    upstream: <ptr to link>,    // ~ potentially ~ exists, meaning that this link bridge to that link, that link is 'up' one level
    inputs: [                   // a list of inputs, each of which:
      {
        type: <string>,             // the type, types are strings 4 js
        name: <string>,             // the name of the input,
        ind: <number>,              // own indice of input, in array of inputs
        parent: <ptr to def>,       // so def.inputs[n].parent is circular,
        is: 'input',                // statement of truth
        conn: [              // routing diagram
          <ptr to output>,             // to-which-is-connected,
          <ptr to output>,             // n=2
        ]
      },
      { etc... }
    ],
    outputs: [                  // a list of outputs, each of which:
      {
        type: <string>,             // the type, types are strings 4 js
        name: <string>,             // the name of the output,
        ind: <number>,              // own indice of output, in array of outputs
        parent: <ptr to def>,       // so def.outputs[n].parent is circular,
        is: 'output',               // statement of truth
        conn: [              // routing diagram
          <ptr to input>,             // to-which-is-connected,
          <ptr to input>,             // n=2
        ]
      },
      { etc... }
    ],
    states: [                    // a list of states, each of which:
      {
        type: <string>,             // the type,
        name: <string>,             // name of the state,
        ind: <number>,              // own indice of
        parent: <ptr to def>,       // so def.states[n].parent is circular,
        is: 'state',                // statement of truth
        value: <type><value>        // it's current value, js native
      },
      { etc... }
    ]
  }
  */

  /* ---------------------------    ---------------------------- */
  /* -------------------- RENDER INTERFACE --------------------- */
  /* ---------------------------    ---------------------------- */

  // we interface to the UI by setting these flags...
  // when we make big changes, just nuke 'em
  // mostly, this is useful for any deleted hunks, or the refresh
  this.hasCompleteRedraw = false
  this.setCompleteRedraw = () => {
    window.check = true
    this.hasCompleteRedraw = true
  }

  // for any updates to wires, we set this:
  // atm, this wipes and redraws all connectivity in the context
  this.hasConnectivityChange = false
  let setConnectivityChange = (output) => {
    window.check = true
    this.hasConnectivityChange = true
  }

  // this means one or more of the defs have been updated:
  // meaning names of any component, or shape of the def: new i/o/s
  // *counter to this* we have state updates, these fire
  // callbacks - this is inconsistent, but change can come later
  this.hasDefUpdate = false
  let setDefUpdate = (index) => {
    window.check = true
    // we have one,
    this.hasDefUpdate = true
    // it's this one
    if (defs[index]) {
      defs[index].hasUpdate = true
    } else {
      console.warn("update flag set for nonexistent def")
    }
  }

  // an oddity, we occasionally change positions,
  this.hasDefPositionUpdate = false
  let setDefReposition = (index) => {
    window.check = true
    this.hasDefPositionUpdate = true
    if (defs[index]) {
      defs[index].hasPositionUpdate = true
    } else {
      console.warn("position update flag set for nonexistent def")
    }
  }

  /* ---------------------------    ---------------------------- */
  /* -------------------- INIT, LISTENERS ---------------------- */
  /* ---------------------------    ---------------------------- */

  // really shouldn't have much to do here
  this.init = () => {
    // or, apparently, anything
  }
  // END INIT CODE

  /* ---------------------------    ---------------------------- */
  /* ------------------- SERIAL -> HOTMESSES ------------------- */
  /* ---------------------------    ---------------------------- */

  let deserializeNameType = (arr, bytes, i) => {
    let nameresp = MSGS.readFrom(bytes, i, 'string')
    let typeresp = MSGS.readFrom(bytes, i + nameresp.increment, 'string')
    arr.push({
      name: nameresp.item,
      type: typeresp.item
    })
    return nameresp.increment + typeresp.increment
  }

  // this is where we read-in definitions from serialized messages, and make the
  // objects we'll otherwise operate - our internal representation of them
  let deserializeDef = (bytes, start, debug) => {
    // deserialize
    let def = {
      type: null,
      name: null,
      ind: null,
    }
    // inputs, outputs, state
    def.inputs = new Array()
    def.outputs = new Array()
    def.states = new Array()
    // hold,
    let temp
    // starting at 2, msgs[0] is 'hnkalive'
    let i = start
    // lets write a goddang real structure an object (def) with mirror type dom and access fn's ...
    // this will make building better and better code mucho bueno
    // ripperoni,
    try {
      outer: while (i < bytes.length) {
        switch (bytes[i]) {
          case HK.IND:
            i += 1
            temp = MSGS.readFrom(bytes, i, 'uint16')
            def.ind = temp.item
            i += temp.increment
            break
          case HK.TYPE:
            i += 1
            temp = MSGS.readFrom(bytes, i, 'string')
            def.type = temp.item
            i += temp.increment
            break
          case HK.NAME:
            i += 1
            temp = MSGS.readFrom(bytes, i, 'string')
            def.name = temp.item
            i += temp.increment
            break
          case HK.INPUT:
            i += 1
            // expecting two strings here, name and then type
            i += deserializeNameType(def.inputs, bytes, i)
            break
          case HK.OUTPUT:
            i += 1
            i += deserializeNameType(def.outputs, bytes, i)
            if (bytes[i] === HK.CONNECTIONS) {
              let cn = 0
              i++
              def.outputs[def.outputs.length - 1].startupConn = []
              //console.log("HK B4 CONN")
              while (bytes[i] === HK.CONNECTION) {
                //console.log("HK CONNECTION");
                let frdarr = bytes.slice(i + 1, i + 3)
                //console.log("HK bytes for inHunkIndice", frdarr)
                let fbtarr = Uint8Array.from(frdarr)
                let fvlarr = new Uint16Array(fbtarr.buffer)
                i += 2
                def.outputs[def.outputs.length - 1].startupConn[cn] = [fvlarr[0], bytes[i + 1]]
                i += 2
              }
            }
            break
          case HK.STATE:
            i += 1
            i += deserializeNameType(def.states, bytes, i)
            // ok, and the value should trail
            // we don't *need* to know the type, could just read by the key, but,
            //console.log(`${this.name} STATE READ from ${i}`, bytes[i], bytes);
            temp = MSGS.readFrom(bytes, i, def.states[def.states.length - 1].type)
            //console.log(`${this.name} TEMP`, temp);
            def.states[def.states.length - 1].value = temp.item
            i += temp.increment
            break
          default:
            console.log('def so far', def)
            console.log('bytes', bytes)
            throw new Error(`unexpected key encountered during hunk deserialization at position ${i}: ${bytes[i]}`)
            break outer
        }
      }
    }
    catch (err) {
      console.warn("deserializing hunk, caught err in try/catch:")
      console.error(err)
      console.log('def so far', def)
      console.log('bytes', bytes)
      throw new Error("halting!")
    }
    // a check,
    if (debug) console.log(`broke outer, len at ${bytes.length}, i at ${i}`)
    // we should be able to kind of pick-thru and append based on,
    if (debug) console.log('def apres deserialize', def, 'from bytes', bytes)
    // we gucc ?
    if (def.name == null || def.type == null) {
      throw new Error('null def, wyd?')
    }
    // and we keep also ah hookup guide
    for (let ip in def.inputs) {
      def.inputs[ip].ind = ip
      def.inputs[ip].parent = def
      def.inputs[ip].is = 'input'
      def.inputs[ip].conn = []
    }

    // we guarantee that outputs have an array of connections: they may be empty...
    for (let op in def.outputs) {
      def.outputs[op].ind = op
      def.outputs[op].parent = def
      def.outputs[op].is = 'output'
      def.outputs[op].conn = []
    }

    // aaand states can nav nav too
    for (let st in def.states) {
      def.states[st].ind = st
      def.states[st].parent = def
      def.states[st].is = 'state'
    }
    // we also guarantee that a definition has a parent, that's us
    // actually, this is a context ... er, a virtual context. semantics
    def.context = this
    if (this == window.tlv) {
      // we are the top level view, this def has a real hunk in same memory space,
      // this is useful to relink, thx
      let hunk = window.hunks[def.ind]
      if (!((hunk.name === def.name) && (hunk.type === def.type))) throw new Error('this should never be the case')
      def.hunk = hunk
      hunk.def = def
    }
    // the indice
    return def
  }

  /* ---------------------------    ---------------------------- */
  /* ------------------------- CLEANUP ------------------------- */
  /* ---------------------------    ---------------------------- */

  // dessicate
  this.cleanDef = (def) => {
    // rm everything ...
    for (let key in def) {
      if (def.hasOwnProperty(key)) {
        if (key == 'type' || key == 'name' || key == 'ind' ||
          key == 'context' || key == 'inputs' || key == 'outputs' || key == 'states' ||
          key == 'hunk' ||
          key == 'interior' || key == 'downstream' || key == 'upstream' || key == 'thru') {
          // these we keep,
        } else {
          delete def[key]
        }
      }
    }
    // same per input / output / state
    for (let ip of def.inputs) {
      for (let key in ip) {
        if (key == 'type' || key == 'name' || key == 'ind' || key == 'parent' || key == 'is' || key == 'conn') {
          // we keep,
        } else {
          delete ip[key]
        }
      }
    }
    for (let op of def.outputs) {
      for (let key in op) {
        if (key == 'type' || key == 'name' || key == 'ind' || key == 'parent' || key == 'is' || key == 'conn') {
          // we keep,
        } else {
          delete op[key]
        }
      }
    }
    for (let st of def.states) {
      for (let key in st) {
        if (key == 'type' || key == 'name' || key == 'value' || key == 'ind' || key == 'parent' || key == 'is') {
          // we keep,
        } else {
          delete st[key]
        }
      }
    }
  }

  /* ---------------------------    ---------------------------- */
  /* ------------------- UPDATE / ADD DEFS --------------------- */
  /* ---------------------------    ---------------------------- */

  let newDef = (def) => {
    def.ind = defs.length
    defs.push(def)
    return def
  } // FIN NEWDEF


  let updateDef = (spec) => {
    console.log('UPDATE')
    // with an update, we maintain link topology
    // with a replace, we wipe link topology,
    // and manager is responsible for updating us with new links
    let def = defs[spec.ind]
    // check name,
    if (def.name !== spec.name) {
      def.name = spec.name
      setDefUpdate(def.ind)
    }
    // check states,
    for (let st in def.states) {
      if (def.states[st].value !== spec.states[st].value) {
        def.states[st].value = spec.states[st].value
        // not too sure about this,
        setDefUpdate(def.ind)
      }
    }
    // get any startup conn attached to existing handle, and watch inputs / outputs
    for (let ip in def.inputs) {
      if (spec.inputs[ip].type !== def.inputs[ip].type) throw new Error('bad time no good') // shouldn't be the case for updates,
      def.inputs[ip].name = spec.inputs[ip].name
    }
    for (let op in def.outputs) {
      if (spec.outputs[op].type !== def.outputs[op].type) throw new Error('also very bad') // same,
      def.outputs[op].name = spec.outputs[op].name
      if (spec.outputs[op].startupConn) def.outputs[op].startupConn = spec.outputs[op].startupConn
    }
    // check link link
    try {
      reconcileStartupConn(def)
    } catch (err) {
      console.error('a bad time was had, with a new definition arriving w/ link topology out of order')
      console.error(err)
    }
    return def
  } // end update

  let rmAllWires = (pdef) => {
    for (let cn of pdef.conn) {
      let rci = cn.conn.findIndex((cand) => {
        return (cand === pdef)
      })
      cn.conn.splice(rci, 1)
    }
    pdef.conn = []
  }

  // this is one of the most awkward bits of the protocol / stack,
  // we assume the thing has more or less completely changed, but
  // it may have just added an input / output / state object.
  // because of link-reconn-issues, we also assume all existing links are gone,
  // and the manager is responsible for reporting back to us all existing wires ...
  let replaceDef = (spec) => {
    // I'll admit I'm not 100% stoked about this, and it's first order for another protocol
    // implementation. graph state is a trip, but seems like it shouldn't have to be. more focus on a tighter
    // underlying rep, and operations on it - i.e. I like .interior() etc as functions that walk the static
    // graph state, rather than things like .context, .parent etc that are static ptrs that might get 'yeeted'
    // as the kids might say
    let od = defs[spec.ind]
    if (od.type != spec.type) throw new Error('this is a bad idea')
    // as a rule, when we replace defs, we unhook everything.
    for (let ip of od.inputs) {
      rmAllWires(ip)
    }
    for (let op of od.outputs) {
      rmAllWires(op)
    }
    // now, since we are occasionally mid-recipe when we replace a def,
    // we don't want to just nuke that old handle - so we really should walk this thing,
    // replacing name, type, etc,
    od.name = spec.name
    od.type = spec.type
    for (let i in spec.inputs) {
      if (od.inputs[i]) {
        od.inputs[i].name = spec.inputs[i].name
        od.inputs[i].type = spec.inputs[i].type
      } else {
        spec.inputs[i].ind = i
        spec.inputs[i].parent = od
        od.inputs.push(spec.inputs[i])
        //console.log(`indice: ${i}, pushed, now len: ${od.inputs.length}`)
      }
    }
    for (let o in spec.outputs) {
      if (od.outputs[o]) {
        od.outputs[o].name = spec.outputs[o].name
        od.outputs[o].type = spec.outputs[o].type
      } else {
        spec.outputs[o].ind = o
        spec.outputs[o].parent = od
        od.outputs.push(spec.outputs[o])
        //console.log(`indice: ${o}, pushed, now len: ${od.outputs.length}`)
      }
    }
    for (let s in spec.states) {
      if (od.states[s]) {
        od.states[s].name = spec.states[s].name
        od.states[s].type = spec.states[s].type
        od.states[s].value = spec.states[s].value
      } else {
        od.states.push(spec.states[s])
      }
    }
    this.setCompleteRedraw()
    // occasionally useful,
    return od
    /*
    defs[spec.ind] = spec
    this.setCompleteRedraw()
    return spec
    */
  }

  /* ---------------------------    ---------------------------- */
  /* ---------------------- REMOVE DEFS ------------------------ */
  /* ---------------------------    ---------------------------- */

  let unhookAllConnections = (def) => {
    for (let op of def.outputs) {
      for (let cn of op.conn) {
        // 'cn' here is the input that is cn to this op,
        // want to dl this from cn.connections[ind]
        let ind = cn.conn.findIndex((cand) => {
          return (cand === op)
        })
        if (ind >= 0) {
          cn.conn.splice(ind, 1)
        } else {
          throw new Error('found broken graph, while rming connections')
        }
      }
    }
    for (let ip of def.inputs) {
      for (let cn of ip.conn) {
        // 'cn' here is the input that is cn to this op,
        // want to dl this from cn.connections[ind]
        let ind = cn.conn.findIndex((cand) => {
          return (cand === ip)
        })
        if (ind >= 0) {
          cn.conn.splice(ind, 1)
        } else {
          throw new Error('found broken graph, while rming connections')
        }
      }
    }
  }

  let removeDef = (index) => {
    // it's gone from remote manager, as are all links cn to it ... rm all here
    let od = defs[index]
    if (od === undefined) throw new Error('no hunk to delete!')
    // now we need to pull each of it's inputs / outputs connections,
    // and rm those elements from any reciprocal arrays
    unhookAllConnections(od)
    // then we rm our handle to it,
    defs.splice(index, 1)
    // and now our .ind properties are untruthful, update those
    onDefReorder()
    // and x2thegrave
    this.setCompleteRedraw()
  }

  let onDefReorder = () => {
    // since we attach the .ind property to each definition,
    // whenever we modify that array we need to update these elements' values
    for (let ind in defs) {
      if (defs[ind].ind !== parseInt(ind)) {
        defs[ind].ind = parseInt(ind)
      }
    }
  }

  /* ---------------------------    ---------------------------- */
  /* ------------------- PUT AND CUT LINKS --------------------- */
  /* ---------------------------    ---------------------------- */

  // I can imagine this causing issues, but we should only have to do this
  // once we're done refreshing something -
  let reconcileStartupConn = (def) => {
    console.log(`reconciling ${def.name}`)
    for (let op of def.outputs) {
      if (!op.startupConn) continue
      for (let cn of op.startupConn) {
        // does it already exist?
        if (op.conn.includes(defs[cn[0]].inputs[cn[1]])) continue
        putLink(def.ind, op.ind, cn[0], cn[1])
      }
      delete op.startupConn
    }
  }

  let putLink = (outInd, outputInd, inInd, inputInd) => {
    try {
      let outputDef = defs[outInd].outputs[outputInd]
      let inputDef = defs[inInd].inputs[inputInd]
      outputDef.conn.push(inputDef)
      inputDef.conn.push(outputDef)
      setConnectivityChange(outputDef)
    } catch (err) {
      console.error('ERR at putlink', err)
      console.error(`outInd ${outInd}, outputInd ${outputInd}, inInd ${inInd}, inputInd ${inputInd}`)
      console.error(`from ${this.name} having manager ${this.interpreterName}`)
      return false
    }
    return true
  }

  // surgery is harder,
  let cutLink = (outInd, outputInd, inInd, inputInd) => {
    try {
      let outputDef = defs[outInd].outputs[outputInd]
      let inputDef = defs[inInd].inputs[inputInd]
      // rm from connections,
      let cnOpIndice = outputDef.conn.findIndex((cand) => {
        return (cand === inputDef)
      })
      outputDef.conn.splice(cnOpIndice, 1)
      // and from cn on the input side, which *should* exist
      let cnIpIndice = inputDef.conn.findIndex((cand) => {
        return (cand === outputDef)
      })
      inputDef.conn.splice(cnIpIndice, 1)
      setConnectivityChange(outInd)
      return true
    } catch (err) {
      console.error('ERR at rmlink', err)
      return false
    }
    return false
  }

  /* ---------------------------    ---------------------------- */
  /* ----------------- HOTMESSES -> SERIALMSGS ----------------- */
  /* ---------------------------    ---------------------------- */

  // the promise land; these bottle the unit requests we might make to
  // a remote manager into JS promises, meaning they can be used
  // in chain-linked async functions, or await-reply UI handles, etc.

  // hello is a ping,
  this.sayHelloToManager = () => {
    return new Promise((resolve, reject) => {
      let helloTime = performance.now()
      promiseThis([MK.HELLO], () => {
        resolve(performance.now() - helloTime)
      }, (errmsg) => {
        reject(errmsg)
      })
    })
  }

  // we can ask for top-level descriptions, or hunks descriptions (by index)
  this.queryManager = (ind) => {
    return new Promise((resolve, reject) => {
      if (ind !== undefined) {
        let req = [MK.QUERY]
        MSGS.writeTo(req, ind, 'uint16')
        promiseThis(req, (def) => {
          resolve(def)
        }, (errmsg) => {
          reject(errmsg)
        })
      } else {
        promiseThis([MK.QUERY], (brief) => {
          resolve(brief)
        }, (errmsg) => {
          reject(errmsg)
        })
      }
    })
  }

  // we can ask for a list of available new-stuff to add,
  this.requestListAvail = (prefix) => {
    // todo: where prefix is like dir/subdir/etc .. to heirarchy dive
    if (prefix) console.error('list query with prefixes not written...')
    return new Promise((resolve, reject) => {
      promiseThis([MK.REQLISTAVAIL], (list) => {
        resolve(list)
      }, (errmsg) => {
        reject(errmsg)
      })
    })
  }

  // ask to add one,
  this.requestAddHunk = (type, name, states) => {
    //console.log('type, states, name', type, states, name)
    return new Promise((resolve, reject) => {
      let msg = [MK.REQADDHUNK]
      MSGS.writeTo(msg, type, 'string')
      // when requesting a hunk with state option,
      // we just throw down (similar to descriptions) a list of
      // names, types, values ...
      // these will not always be used, but this is an
      // exercise in consistency at the moment
      // in an array
      // if its named,
      if (name) {
        msg.push(HK.NAME)
        MSGS.writeTo(msg, name, 'string')
      }
      if (states) {
        for (let st of states) {
          msg.push(HK.STATE)
          // later, of course, you have discovered that you don't need the 2nd string here...
          let msgbegin = msg.length
          MSGS.writeTo(msg, st.name, 'string')
          MSGS.writeTo(msg, st.type, 'string')
          MSGS.writeTo(msg, st.value, st.type)
          /*
          if (this.interpreterName === "ponyo" && type === "link") {
            console.log(`STATE SER FOR: ${st.value} into ${name}`)
            console.log(msg.slice(msgbegin))
          }
          */
        }
      }
      promiseThis(msg, (def) => {
        resolve(def)
      }, (errmsg) => {
        // since this is a remote call,
        // this will be a pure-text error from the manager,
        // whether it's remote or local ...
        reject(errmsg)
      })
    })
  }

  // to name them
  this.requestRenameHunk = (ind, name) => {
    return new Promise((resolve, reject) => {
      let msg = [MK.REQNAMECHANGE]
      MSGS.writeTo(msg, ind, 'uint16')
      MSGS.writeTo(msg, name, 'string')
      promiseThis(msg, (def) => {
        resolve(def)
      }, (errmsg) => {
        reject(errmsg)
      })
    })
  }

  // to change their state
  this.requestStateChange = (state, value) => {
    return new Promise((resolve, reject) => {
      let msg = [MK.REQSTATECHANGE]
      MSGS.writeTo(msg, state.parent.ind, 'uint16')
      MSGS.writeTo(msg, state.ind, 'uint8')
      try {
        MSGS.writeTo(msg, value, state.type)
      } catch (err) {
        reject(err)
        console.log('that type err:', err)
      }
      promiseThis(msg, (stdef) => {
        resolve(stdef)
      }, (errmsg) => {
        reject(errmsg)
      })
    })
  }

  // or remove them,
  this.requestRemoveHunk = (ind) => {
    return new Promise((resolve, reject) => {
      let msg = [MK.REQRMHUNK]
      MSGS.writeTo(msg, ind, 'uint16')
      promiseThis(msg, (rmid) => {
        resolve(rmid)
      }, (errmsg) => {
        console.error('error while removing hunk', errmsg)
        reject(errmsg)
      })
    })
  }

  // and we can add links
  this.requestAddLink = (output, input) => {
    return this.requestAddLinkLikeACaveman(output.parent.ind, output.ind, input.parent.ind, input.ind)
  }

  this.requestAddLinkLikeACaveman = (outInd, outputInd, inInd, inputInd) => {
    return new Promise((resolve, reject) => {
      let msg = [MK.REQADDLINK]
      MSGS.writeTo(msg, parseInt(outInd), 'uint16')
      MSGS.writeTo(msg, parseInt(outputInd), 'uint8')
      MSGS.writeTo(msg, parseInt(inInd), 'uint16')
      MSGS.writeTo(msg, parseInt(inputInd), 'uint8')
      promiseThis(msg, (link) => {
        resolve(link)
      }, (errmsg) => {
        reject(errmsg)
      })
    })
  }

  // or remove them
  this.requestRemoveLink = (output, input) => {
    return new Promise((resolve, reject) => {
      let msg = [MK.REQRMLINK]
      MSGS.writeTo(msg, output.parent.ind, 'uint16')
      MSGS.writeTo(msg, output.ind, 'uint8')
      MSGS.writeTo(msg, input.parent.ind, 'uint16')
      MSGS.writeTo(msg, input.ind, 'uint8')
      promiseThis(msg, (link) => {
        resolve(link)
      }, (errmsg) => {
        reject(errmsg)
      })
    })
  }

  /* ---------------------------    ---------------------------- */
  /* ------------------------ RECEPIES ------------------------- */
  /* ---------------------------    ---------------------------- */

  // with recepies, we take the atomic units above and do larger order operations

  // wipes current state from remote, polls from zero
  /*
  so, at the moment the replaceDef that occurs when we go to build a route
  manages to wipe some of the state out of the route - probably we are
  cleaning defs in our view, that is erasing graph knowledge. this means
  we should probably prune the cleanup, and try to make some careful sense of what's going on
  when we run the redrawing op as well ...
  ... yeah, recall that expanding / contracting w/e is stateful at the button. this is indeed
  a new days' can of worms
  ... perhaps the way to do this is to set a global .norefresh flag, see if the route building alone works
  and then handle etcs afterwards, i.e. probably just cleaning up .clean() (ha)
  */
  this.refresh = () => {
    console.warn(`REFRESH ${this.name}`)
    // well, I won't say it's clean or beautiful, but we are interested in keeping the
    // positions of things intact, so I am here for this:
    let psns = []
    for (let df of this.defs) {
      if (df.position) {
        psns.push({
          ind: df.ind,
          position: df.position
        })
      }
    }
    // now we wipe,
    return new Promise(async (resolve, reject) => {
      // we want to run this down levels,
      for (let df of this.defs) {
        if (df.type == 'link') {
          if (df.interior()) await df.interior().refresh()
        }
      }
      // wipe ya docs, and ask yonder manager for a complete description
      // setting ahn JS array's length to zero effectively erases it, unless
      // there are other references to them someplace ... then GC will keepalive
      defs.length = 0
      // here we can set flag for complete redraw, as we've just tabula'd the table,
      this.setCompleteRedraw()
      // hello first,
      this.sayHelloToManager().then(() => {
        this.queryManager().then((brief) => {
          // console.log('BRIEF', brief)
          // should have a list then, do that brief then ?
          let nh = brief.numHunks
          // the repeater / promise walker
          let cbnh = (n) => {
            this.queryManager(n).then((def) => {
              // try to restore positions,
              // this assumes complete success.
              let cnd = psns.find((cand) => {
                return (cand.ind == def.ind)
              })
              if (cnd) {
                // reset this,
                def.position = cnd.position
                // and redraw it,
                setDefReposition(def.ind)
              }
              // ok,
              n++
              if (n < nh) {
                // case not-complete, so continue
                cbnh(n)
              } else {
                // we have all of the hunks now, so we can read out their connections
                // into our global-info-req'd pointer representation
                // this one's a bit odd, and we should only have
                // to do it in this case, but a potential bugfarm
                for (let df of defs) {
                  reconcileStartupConn(df)
                }
                resolve(this)
              }
            }).catch((err) => {
              console.error(err)
              reject(err)
            })
          }
          // the kickoff,
          cbnh(0)
        })
      })
    })
  }

  // deletes, adds again. in JS, this means we reload from server source,
  // in compiled langs this will just reset to default states
  this.reloadHunk = async (indice, restoreState) => {
    // one note: in compiled langs, or for singletons, adding new *before*
    // deleting the old will f us right up
    // I see though that this is written so that we can avoid storing some local
    // and temporary rep. of link states
    return new Promise((resolve, reject) => {
      // reloads from source (devtool) if restoreState is set,
      let od = this.defs[indice]
      // we're not going to splice this into place, we're just going to
      // throw another one on top.
      this.requestAddHunk(od.type).then(async (def) => {
        // restore position if we have it,
        if (od.position) def.position = od.position
        // not sure about doing this now / later ...
        setDefReposition(def.ind)
        // let's compare these ? for matching inputs, outputs, reconnect:
        let matching = (odp, ndp, ind) => {
          if (odp[ind]) {
            if (odp[ind].name === ndp[ind].name && odp[ind].type === ndp[ind].type) return true
          } else {
            return false
          }
        }
        // these links are all added on a promise, meaning we aren't sure (by the time this runs to completion)
        // whether or not they are successful...
        // with better view -> manager integration, this would be mucho easier
        for (let ip in def.inputs) {
          if (matching(od.inputs, def.inputs, ip)) {
            for (let cn of od.inputs[ip].conn) {
              await this.requestAddLink(cn, def.inputs[ip])
            }
          }
        }
        for (let op in def.outputs) {
          if (matching(od.outputs, def.outputs, op)) {
            for (let cn of od.outputs[op].conn) {
              await this.requestAddLink(def.outputs[op], cn)
            }
          }
        }
        // we should wait for everything all async like, but
        this.requestRemoveHunk(od.ind).then(() => {
          resolve(def)
        })
      }).catch((err) => {
        reject(err)
      })
      // let's load one in w/ the changes ... if we can do this we'll go thru the rest
    })
  }

  this.save = () => {
    return new Promise(async (resolve, reject) => {
      // now is the thyme to write-out our defs,
      let patch = {
        // cerntakt
        interpreterName: this.interpreterName,
        interpreterVersion: this.interpreterVersion,
        open: this.open,
        size: this.size,
        hunks: []
      }
      for (let df of defs) {
        try {
          // want to scrape out just-the-necessary bits
          // proposal is to save minimum viable stuff
          let hnk = {
            name: df.name,
            type: df.type,
            inputs: [],
            outputs: [],
            states: []
          }
          for (let st of df.states) {
            hnk.states.push({
              name: st.name,
              type: st.type,
              value: st.value
            })
          }
          for (let ip of df.inputs) {
            hnk.inputs.push({
              name: ip.name,
              type: ip.type
            })
          }
          for (let op of df.outputs) {
            let oph = {
              name: op.name,
              type: op.type,
              conn: []
            }
            // de-reference to indice-indice type,
            for (let cn of op.conn) {
              //console.log(`SL ${df.name} op ${op.name} push ${cn.parent.ind}, ${cn.ind}`)
              oph.conn.push([parseInt(cn.parent.ind), parseInt(cn.ind)])
            }
            hnk.outputs.push(oph)
          }
          if (df.position) hnk.position = df.position
          if (df.type == 'link') {
            if (df.interior()) {
              // WARN: the link-state hack, needs link work / protocol work 
              hnk.states[0].value = false
              hnk.contains = await df.interior().save() // this should recurse, yeah ?
            }
          }
          patch.hunks.push(hnk)
        } catch (err) {
          console.error(err)
          reject('trouble saving patch')
        }
      }
      resolve(patch)
    })
  }

  /* ---------------------------    ---------------------------- */
  /* --------------------- SYSTEM RESTORE ---------------------- */
  /* ---------------------------    ---------------------------- */

  let mergeHunkList = (patch, debug) => {
    return new Promise(async (resolve, reject) => {
      // now wrip em out
      let loadedDefsList = []
      let nh = patch.hunks.length
      // walk w/ this, use n for hunk-indice,
      let recursor = async (n) => {
        if (debug) console.log(`hunkmerge, at ${n}`)
        // the hunk we want,
        let spec = patch.hunks[n]
        let existing = defs[n]
        let result = null
        if (existing !== undefined) {
          // have one in this position already,
          if (existing.type !== spec.type) reject('mismatched initial state')
          for (let st in existing.states) {
            if (!spec.states[st]) break // existence check, type check ->
            if ((spec.states[st].type !== existing.states[st].type) || (spec.states[st].name !== existing.states[st].name)) continue
            // try to change, but continue nonetheless
            try {
              if (debug) console.log(`hunkmerge, at ${n}, to swap in state ${st}`)
              await this.requestStateChange(existing.states[st], spec.states[st].value)
            } catch (err) {
              console.error("couldn't swap ah state back to spec from this patch, continueing as is")
            }
          }
          result = existing
        } else {
          // donot have one here yet, pls add it:
          if (debug) console.log(`hunkmerge, at ${n}, to add new hunk ${spec.type}`)
          await this.requestAddHunk(spec.type, spec.name, spec.states)
          if (debug) console.log(`hunkmerge, at ${n}, def loaded, is ${defs[n].name}`)
          loadedDefsList.push(defs[n])
          result = defs[n]
        }
        // ok, done w/ adding and states:
        if (!result) reject(`something has gone wrong while merging hunks, see http://shrugguy.com for more info`)
        if (spec.position) {
          result.position = {}
          result.position.x = parseInt(spec.position.x)
          result.position.y = parseInt(spec.position.y)
        }
        setDefReposition(n)
        // restore the position,
        if ((n++) < nh - 1) {
          recursor(n++)
        } else {
          resolve(loadedDefsList)
        }
      } // end recursor,
      // start the recursion:
      recursor(0)
    })
  }

  let mergeLinkList = (patch, debug) => {
    return new Promise(async (resolve, reject) => {
      let nh = patch.hunks.length
      let recursor = async (n) => {
        if (debug) console.log(`MLL for ${patch.hunks[n].name}`)
        // we want to walk the list of outputs in the patch, and reconn.
        for (let op in patch.hunks[n].outputs) {
          if (debug) console.log(`MLL for ${patch.hunks[n].outputs[op].name}`)
          if (!defs[n].outputs[op]) {
            if (debug) console.log(`no output for reciprocal actual-def`)
            break // if the thing-actually-loaded doesn't have an output here, pass
          }
          if (!patch.hunks[n].outputs[op].conn) {
            if (debug) console.log(`no conn for patch op`)
            continue // during some serializations / deserializations, empty arrays are culled, so
          }
          for (let cn of patch.hunks[n].outputs[op].conn) {
            if (debug) console.log(`conn for ${patch.hunks[n].name}, output ${op}, to ${patch.hunks[cn[0]].name}, ${cn[1]}`)
            // now! we would like to see about whether / not this link already exist.
            // it's possible to double up wires, so let's try not to ...
            let existenceProof = defs[n].outputs[op].conn.find((cand) => {
              // recall: the candidate is ahn input, in this outputs' list of connections
              return ((cand.ind == cn[1]) && (cand.parent.ind == cn[0]))
            })
            if (existenceProof) {
              if (debug) console.log('link exists, continueing')
              continue
            } else {
              // here is case to put error handling in the ll, at addlink,
              // then just ignore the ones we miss ...
              try {
                if (debug) console.log('go for add', n, op, 'to', cn)
                await this.requestAddLinkLikeACaveman(n, op, cn[0], cn[1])
              } catch (err) {
                console.error('passing by this erroneous link-addition during link-list merging')
                console.error(err)
              }
            }
          }
        } // end for-each-output
        n++
        if (n < nh) {
          recursor(n)
        } else {
          resolve()
        }
      } // end recursor,
      recursor(0)
    })
  }

  this.reinstate = async (system, debug) => {
    console.warn(`${this.name} begins reinstate`)
    // ensure we are up-to-date w/ current state,
    await this.refresh()
    // and and,
    this.open = system.open
    this.size = system.size
    // ok,
    return new Promise(async (resolve, reject) => {
      try {
        await mergeHunkList(system, debug)
        await mergeLinkList(system, debug)
        console.log(`MLL COMPLETE`)
        for (let s in system.hunks) {
          let spec = system.hunks[s]
          if (spec.contains) {
            // find the context for it (this should be the matched link)
            let interior = defs[s].interior()
            if (!interior) throw new Error('cannot grab a context for this subsection of the system', spec.contains)
            await interior.reinstate(spec.contains)
          }
        }
        console.warn(`${this.name} finish reinstate`)
        resolve()
      } catch (err) {
        console.error(err)
        reject(err)
      }
    })
  }

  /* ---------------------------    ---------------------------- */
  /* ------------------------- PIPING -------------------------- */
  /* ---------------------------    ---------------------------- */

  // return inputdef-to-which connected,
  // assume always 1d path, bail if not
  // if tolink, look only for next link - input at positin 1, this is mgrmsgs,
  this.trace = (opdef, tolink, debug) => {
    if(debug) console.log('TRACE LEVEL', opdef)
    // for now we are 1d, cannot traverse links,
    if (opdef.conn.length !== 1){
      if (debug) console.log('TRACE NO CONN, RET NULL')
      return
    }
    // trace is fun bc it recurses thru itself,
    let next = opdef.conn[0]
    if (next.parent.type === 'link') {
      if (tolink && next.ind == 1) {
        if(debug) console.log('TRACE RETURN', next)
        return next
      } else {
        try {
          let recip = next.parent.thru()
          if (!recip){
            if(debug) console.log('TRACE NO RECIP, NEXT', next)
            return next // no next-link,
          }
          if (recip.outputs[next.ind]) {
            if(debug) console.log('TRACE RECIP, DIVE')
            return this.trace(recip.outputs[next.ind], tolink, debug) // recurse across,
          } else {
            if(debug) console.log('recip', recip)
            if(debug) console.log('TRACE RECIP, NO NEXT OUTPUT')
          }
        } catch (err) {
          console.error(next.parent)
          console.error(err)
          window.halt()
        }
      }
    } else {
      if (debug) console.log('TRACE NEXT != LINK, RETURN', next)
      return next
    }
  }

  let addLinkHelpers = (ldef) => {
    // for any link, find / build a route up to a view, from its' 1st input / output - that are assumed to be manager
    // message ports to the manager within that link...
    ldef.interior = () => {
      let tr = this.trace(ldef.outputs[1]) // option to run modified trace to find first link
      if (tr) {
        if (tr.parent.type == 'view') {
          return tr.parent.hunk
        }
        return
      }
    }

    ldef.downstream = () => {
      let inside = ldef.interior()
      if (!inside) return
      try {
        // some chance we have a view non-refreshed, etc
        let reciprocal = inside.defs[ldef.states[1].value]
        if (reciprocal.type == 'link') return reciprocal
      } catch (err) {
        // pass
        return
      }
    }

    ldef.upstream = (debug) => {
      let outside = this.exterior(debug) // get the link that this is 'within' ... 'this' is the view, recall
      if (!outside) {
        if (debug) console.log('ldef.upstream return null: no exterior')
        return
      }
      try {
        let reciprocal = outside.context.defs[ldef.states[1].value]
        if (reciprocal.type == 'link') {
          if (debug) console.log('ldef.upstream finds reciprocal link, returning')
          return reciprocal
        } else {
          if (debug) console.log('ldef.upstream finds reciprocal, not a link')
        }
      } catch (err) {
        console.error(err)
        return
      }
    }

    ldef.thru = () => {
      let up = ldef.upstream()
      if (up) return up
      let down = ldef.downstream()
      if (down) return down
    }

  } // end link helpers,

  // query to find the link-definition to the link that this view is 'inside of'
  // i.e. which link is on the upstream side of the context for which we are an MVC.
  // ... pretty straightfoward if you ask me
  this.exterior = (debug) => {
    // skip tlv,
    if (this == window.tlv) {
      if (debug) console.log(`${this.name} this.exterior return null: is top level`)
      return
    }
    if (!this.def) {
      if (debug) console.log(`${this.name}.exterior return null: has not def reciprocal`)
      return
    }
    // here's a funny overlay: this.outputs[0] != this.def outputs ...
    let res = this.trace(this.def.outputs[0], true, debug)
    if (res) {
      if (debug) console.log(`${this.name} this.exterior returns a trace,`)
      return res.parent
    } else {
      if (debug) console.log(`${this.name} this.exterior returns null: trace returns no result`)
      return
    }
  }

  this.buildLinkContext = (ldef) => {
    return new Promise(async (resolve, reject) => {
      let current = ldef.interior()
      if (current) {
        resolve(current)
      } else {
        // build it, cn it, continue,
        try {
          let vdef = await window.tlv.requestAddHunk('view')
          await this.buildRoute(vdef.outputs[0], ldef.inputs[1])
          await this.buildRoute(ldef.outputs[1], vdef.inputs[0])
          let view = vdef.hunk // views are always toplevel hunks, so they always have these handles
          resolve(ldef.interior()) // now that we're all hooked up, this should run no problem
        } catch (err) {
          console.error(err)
          reject()
        }
      }
    })
  }

  // this is really kind of ah
  let traceClone = (trace) => {
    let nt = []
    for (let t of trace) {
      nt.push({
        entrance: t.entrance,
        exit: t.exit
      })
    }
    return nt
  }

  this.buildRoute = (opdef, ipdef, debug) => {
    debug = true
    // *doesn't handle* but could: half-existing routes, etc ?
    return new Promise(async (resolve, reject) => {
      // already exists ?
      let tr = this.trace(opdef)
      if (tr === ipdef) {
        resolve()
        return
      } else if (tr != undefined) {
        reject(`trace returns existing route for alternate input, pass`)
      }
      // are these in the same context? if so, this is trivial,
      if (opdef.parent.context === ipdef.parent.context) {
        opdef.parent.context.requestAddLink(opdef, ipdef).then(() => {
          resolve()
        }).catch((err) => {
          reject(err)
        })
      } else {
        // otherwise we have work to do, first we make a route,
        let route = []
        // ok, we are now going to try to build a route between *contexts*
        // mapping a trace of links between them,
        let recursor = (context, entrance, trace) => {
          if(debug) console.log('BR Recurse', context, entrance)
          for (let df of context.defs) {
            if (df.type === 'link' && df !== entrance) {
              if (df.thru()) {
                // bit of a lazy break: we could do a trace-copy,
                // manually copying each element ... then we can retain ptrs and
                // avoid handling all of these names later...
                let uniquetrace = traceClone(trace)
                uniquetrace.push({
                  entrance: df,
                  exit: df.thru()
                })
                // if this is it, trace building is complete,
                if (df.thru().context == ipdef.parent.context) {
                  // other branches will run to completion, route will remain...
                  if(debug) console.log('ROUTE COMPLETE')
                  route = traceClone(uniquetrace)
                } else {
                  if(debug) console.log('RECURSE WITHIN')
                  recursor(df.thru().context, df.thru(), traceClone(uniquetrace))
                }
              } else {
                //
              }
            }
          }
        } // end recursor,
        // kickoff,
        recursor(opdef.parent.context, null, [])
        // ok, once this runs to completion, we should have
        if(debug) console.log('BR: ROUTE IS', route)
        // presuming we will be able to build a route, we should pick a type
        // it's more likely that higher level languages will have a broader set of
        // type conversions, and at the moment systems are typically higher level at higher orders
        // doing this next step well is a job for new protocol, but let's just pick the lowest level stack to build with
        let type
        if (route[0].entrance.upstream) {
          // we are starting-low, going-up
          type = opdef.type
        } else {
          type = ipdef.type
        }
        // now we can resolve that route s/o asyncness
        // opdef -> [ entrance (link) | ~ network ~ | (link) exit] -> [...] -> [ entrance (link) | ~ network ~ | (link) exit] -> ipdef
        // cover output -> first link,
        try {
          let gateway = route[0].entrance // the first link,
          let gatelist = gateway.states[2].value
          gatelist += `, at_${gateway.outputs.length} (${type})`
          if(debug) console.log('BR: OPEN GATEWAY')
          await gateway.context.requestStateChange(gateway.states[2], gatelist)
          // cover interiors,
          for (let p = 0; p < route.length - 1; p++) {
            let exit = route[p].exit
            let entrance = route[p + 1].entrance
            let context = route[p].exit.context
            let exl = exit.states[3].value
            exl += `, at_${exit.outputs.length} (${type})`
            if(debug) console.log('BR: OPEN NEXT EXIT')
            await context.requestStateChange(exit.states[3], exl)
            let enl = entrance.states[2].value
            enl += `, at_${entrance.inputs.length} (${type})`
            if(debug) console.log('BR: OPEN NEXT ENTRANCE')
            await context.requestStateChange(entrance.states[2], enl)
            if(debug) console.log('BR: CONNECT')
            await context.requestAddLink(exit.outputs[exit.outputs.length - 1], entrance.inputs[entrance.inputs.length - 1])
          }
          // cover exit case
          let porthole = route[route.length - 1].exit
          let portlist = porthole.states[3].value
          portlist += `, at_${porthole.outputs.length} (${type})`
          /*
          the eternal recurrence of the pointer statefulness problem
          when we replace, our porthole <ptr to ldef> is no longer the ldef we want or need, it's a dead ptr
          to something that exists nowhere else in the tree
          ok, so, two moves: one: re-reference here,
          two: probably more robust: write replaceDef in such a way that existing ptrs stay all gucci gucci
          ok, ok, party, burrito, etc.
          */
          if(debug) console.log('BR: OPEN PORTHOLE')
          await ipdef.parent.context.requestStateChange(porthole.states[3], portlist)
          //console.warn('porthole', porthole.outputs[porthole.outputs.length - 1], ipdef)
          if(debug) console.log('BR: WIRE PORT')
          await ipdef.parent.context.requestAddLink(porthole.outputs[porthole.outputs.length - 1], ipdef)
          // to avoid piping data into the void, we can do this last ->
          //console.log('gateway', opdef, gateway.inputs[gateway.inputs.length - 1])
          if(debug) console.log('BR: WIRE GATEWAY')
          await gateway.context.requestAddLink(opdef, gateway.inputs[gateway.inputs.length - 1])
          if(debug) console.log('BR: SUCCESS')
          resolve()
        } catch (err) {
          reject(err)
        }
      }
    })
  } // just about ~ 98 lines, not bad ok cold be cleaner esp. at the end there

  /* ---------------------------    ---------------------------- */
  /* --------------------- MESSAGES OUTPUT --------------------- */
  /* ---------------------------    ---------------------------- */

  let outmsgbuffer = new Array()

  let writeMessage = (bytes) => {
    if (!msgsout.io() && outmsgbuffer.length < 1) {
      // str8 shooters
      if (msgverbose) this.log('msg out', bytes)
      msgsout.put(bytes)
    } else {
      // gotta buffer
      outmsgbuffer.push(bytes)
      this.log('VIEW OUTBUFFER LEN', outmsgbuffer.length)
    }
  }

  // it's often very useful tracking completion, so
  let callbackSets = new Array()
  let cbid = 0

  let promiseThis = (bytes, callback, errback) => {
    cbid++
    if (cbid > 254) {
      cbid = 0
    } // could be safe about this, for now we take for granted less than 255 out,
    // remember it
    callbackSets.push({
      key: bytes[0],
      id: cbid,
      callback: callback,
      errback: errback
    })
    // so our header is like - we don't explicitly type the id, it's a byte
    let head = [MK.MSGID, cbid]
    // and we concat this to
    bytes = head.concat(bytes)
    writeMessage(bytes)
  }

  /* ---------------------------    ---------------------------- */
  /* ------------------ MESSAGES INPUT, LOOP ------------------- */
  /* ---------------------------    ---------------------------- */

  // still looking for clear function naming / message naming

  this.loop = () => {
    // THE Q: is it trees or is it inputs / outputs ? ports ...
    if (msgsin.io()) {
      let msg = msgsin.get()
      if (msgverbose) console.log('VIEW RX MSG:', msg)
      if (!Array.isArray(msg)) throw new Error(`view throwing object message, having header ${msg.header}`)
      // ... mirror for views
      let temp
      let inc = 0
      // track the msg id response, and that callback
      let msgid, cbd
      // find the element, set to cbd ref and delete from array
      if (msg[0] === MK.MSGID) {
        msgid = msg[1]
        inc = 2
        // and find the callback we registered
        let cbdi = callbackSets.findIndex((cand) => {
          return cand.id === msgid
        })
        if (cbdi !== -1) {
          cbd = callbackSets[cbdi]
          callbackSets.splice(cbdi, 1)
        }
      }
      // if a msgid, find the callback
      // ok, get the id and increment ...
      switch (msg[inc]) {
        case MK.ERR:
          if (msgverbose) console.log('VIEW MSG is an error')
          let errmsg = MSGS.readFrom(msg, inc + 1, 'string').item
          console.error('manager returns an error', errmsg)
          if (cbd) cbd.errback(errmsg)
          break
        case MK.HELLO:
          if (msgverbose) console.log('VIEW MSG is hello')
          if (cbd) {
            cbd.callback()
          } else {
            console.log(`manager says hello!`)
          }
          break
        case MK.BRIEF:
          if (msgverbose) console.log('VIEW MSG is a manager brief')
          let brief = {}
          inc++
          // reading strings returns item, increment
          temp = MSGS.readFrom(msg, inc, 'string')
          inc += temp.increment
          brief.interpreterName = temp.item
          temp = MSGS.readFrom(msg, inc, 'string')
          inc += temp.increment
          brief.interpreterVersion = temp.item
          temp = MSGS.readFrom(msg, inc, 'uint16')
          inc += temp.increment
          brief.numHunks = temp.item
          // set local,
          this.interpreterName = brief.interpreterName
          this.interpreterVersion = brief.interpreterVersion
          // and write it down
          if (cbd) cbd.callback(brief)
          break
        case MK.LISTOFAVAIL:
          if (msgverbose) console.log('VIEW MSG is a list of available items')
          let stringlist = MSGS.readListFrom(msg, inc + 1, 'string')
          if (cbd) {
            cbd.callback(stringlist)
          } else {
            // this shouldn't happen w/o a request attached
            console.error('something is up !')
          }
          break
        case MK.HUNKALIVE:
          // this can refer to new hunks, and modified hunk descriptions
          // i.e. changing a list of outputs and inputs
          if (msgverbose) console.log('VIEW MSG is a new hunk')
          let spec = deserializeDef(msg, inc + 1, false)
          // so first we check for an existing hunk,
          let nd
          if (defs[spec.ind] === undefined) {
            console.log(`NEW ${this.name} ${spec.name}`)
            nd = newDef(spec)
            //console.log('the nd', nd)
          } else {
            if (verbose) console.log('updating hunk at', spec.ind)
            console.log("UPDATE", spec.ind)
            nd = updateDef(spec)
          }
          // we manage our connectivity to other views,
          if (nd.type == 'link') addLinkHelpers(nd)
          // set the flag, then call the callback
          setDefUpdate(nd.ind)
          // the promise-this callback,
          if (cbd) cbd.callback(nd)
          break
        case MK.HUNKREPLACE:
          // to add inputs or outputs, this is sent, all links are eliminated,
          // and they are sent again by the manager
          if (msgverbose) console.log('VIEW MSG is a hunk replacement')
          let repSpec = deserializeDef(msg, inc + 1, false)
          if (defs[repSpec.ind] === undefined) {
            console.error('received hunk replace for non existent hunk')
            console.error('here is the spec:', repSpec)
          } else {
            console.log(`REPLACED ${this.name}'s ${repSpec.name}`)
            let nd = replaceDef(repSpec)
            if (nd.type == 'link') addLinkHelpers(nd)
            if (cbd) cbd.callback(nd)
          }
          break
        case MK.HUNKSTATECHANGE:
          if (msgverbose) console.log('VIEW MSG is a state change')
          let stchHnkInd = MSGS.readFrom(msg, inc + 1, 'uint16').item
          let stchStInd = MSGS.readFrom(msg, inc + 4, 'uint8').item
          let stDef
          try {
            stDef = defs[stchHnkInd].states[stchStInd]
            let stValUpdate = MSGS.readFrom(msg, inc + 6, stDef.type).item
            stDef.value = stValUpdate
            // how we commune -> ui, write ah reset fn
            if (stDef.reset) stDef.reset()
            if (cbd) cbd.callback(stDef)
            // should we check links here ?
          } catch {
            // this is a likely early arrival of a state change update,
            // meaning we are likely *still* going to rx the def, with the update
            // so ... continue
            // console.warn('state change request cannot find stDef, likely async manager messages, probably ok')
          }
          break
        case MK.HUNKREMOVED:
          if (msgverbose) console.log('VIEW MSG is a hunk to remove')
          let rmid = MSGS.readFrom(msg, inc + 1, 'uint16').item
          removeDef(rmid)
          if (cbd) cbd.callback(rmid)
          // link check ?
          break
        case MK.LINKALIVE: // -> wirealive
          if (msgverbose) console.log('VIEW MSG is a link to put')
          let nlnk = {}
          //console.log("LINK MSG IS", msg)
          nlnk.outInd = MSGS.readFrom(msg, inc + 1, 'uint16').item
          nlnk.outputInd = MSGS.readFrom(msg, inc + 4, 'uint8').item
          nlnk.inInd = MSGS.readFrom(msg, inc + 6, 'uint16').item
          nlnk.inputInd = MSGS.readFrom(msg, inc + 9, 'uint8').item
          if (putLink(nlnk.outInd, nlnk.outputInd, nlnk.inInd, nlnk.inputInd)) {
            if (cbd) cbd.callback(nlnk)
          } else {
            console.error('view could not write that link')
            if (cbd) cbd.errback('err for view to putLink')
          }
          break
        case MK.LINKREMOVED: // wireremoved
          if (msgverbose) console.log('VIEW MSG is a link to cut')
          let rlnk = {}
          rlnk.outInd = MSGS.readFrom(msg, inc + 1, 'uint16').item
          rlnk.outputInd = MSGS.readFrom(msg, inc + 4, 'uint8').item
          rlnk.inInd = MSGS.readFrom(msg, inc + 6, 'uint16').item
          rlnk.inputInd = MSGS.readFrom(msg, inc + 9, 'uint8').item
          if (cutLink(rlnk.outInd, rlnk.outputInd, rlnk.inInd, rlnk.inputInd)) {
            if (cbd) cbd.callback(rlnk)
          } else {
            console.error('err for view to cutLink')
            if (cbd) cbd.errback('err for view to cutLink')
          }
          break
        default:
          throw new Error(`view receives message with no switch: ${msg}, ${msg[inc]}, ${typeof msg[inc]}, ${inc}`)
      } // end msgs switch
    } // end if-have-message

    // MSGS output check,
    if (outmsgbuffer.length > 0) {
      let debug = true
      if (!msgsout.io()) {
        if (debug) {
          let msg = outmsgbuffer.shift()
          if (msgverbose) this.log(`buffer release msg type: ${msg.header}`)
          //console.log(msg.content)
          msgsout.put(msg)
        } else {
          msgsout.put(outmsgbuffer.shift())
        }
      }
    } // end msgs output check

  } // end loop
}
