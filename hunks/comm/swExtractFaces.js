/*
hunks/comm/swExtractFaces.js

pipe from solidworks, delivers array of svgs, from
slice-it add-in

Jake Read at the Center for Bits and Atoms
Shawn Liu at Solidworks
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

export default function SWExtractFaces() {
  Hunkify(this)

  let dtout = this.output('reference', 'svg array')

  // lots!
  let connectionStatus = this.state('boolean', 'connected', false)
  let statusMessage = this.state('string', 'status', '...')
  // conn, and sw conn,
  let addressState = this.state('string', 'address', '127.0.0.1')
  let portState = this.state('number', 'port', 8787)
  let swId = this.state('string', 'swiD', '7772')
  // config ...
  let thickness = this.state('number', 'thickness (mm)', 3.125)

  // this ws is a client,
  let ws = {}
  let url = `ws://127.0.0.1:${portState.value}`

  // we keep an outgoing set,
  let outbuffer = new Array()

  // in case saved when 'open'
  this.init = () => {
    connectionStatus.set(false)
  }

  connectionStatus.onChange = (value) => {
    if (value) {
      startWs()
    } else {
      ws.close()
    }
  }

  let startWs = () => {
    // manager calls this once
    // it is loaded and state is updated (from program)
    url = 'ws://' + addressState.value + ':' + portState.value
    this.log(`attempt start ws at ${url}`)
    ws = new WebSocket(url)
    ws.binaryType = "arraybuffer"
    ws.onopen = (evt) => {
      this.log('ws opened')
      statusMessage.set('open')
      connectionStatus.set(true)
      // ack to shawn,
      ws.send(JSON.stringify({
        modCmd: 'connect',
        owner: 'mods',
        id: swId.value
      }))
    }
    ws.onerror = (evt) => {
      console.log('ws error:', evt)
      statusMessage.set('error')
      connectionStatus.set(false)
    }
    ws.onclose = (evt) => {
      statusMessage.set('closed')
      connectionStatus.set(false)
    }
    ws.onmessage = (message) => {
      let mobj = JSON.parse(message.data)
      if (true) console.log('sw-ws-faces receives', mobj)
      try {
        if (mobj.swType == 'FaceSVGArray') {
          outbuffer.push(mobj.data[0])
        }
      } catch (err) {
        console.error('bad form on SW connect', err)
      }
    }
    statusMessage.set('ws startup...')
  }

  let extractSVGs = () => {
    let mc = {
      modCmd: "AutoExtractFaces",
      thickness: thickness.value
    }
    if(ws.readyState === 1){
      ws.send(JSON.stringify(mc))
    }
  }

  let extractor = this.state('boolean', 'extract', false)
  extractor.onChange = (value) => {
    extractSVGs()
    extractor.set(false)
  }

  this.loop = () => {
    if(outbuffer.length > 0 && !dtout.io()){
      dtout.put(outbuffer.shift())
    }
  }
}
