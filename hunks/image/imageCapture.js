/*
hunks/image/imagecapture.js

pull image from webcam, use imagecapture interface for fine tuning 

Jake Read at the Center for Bits and Atoms with Neil Gershenfeld and Leo McElroy
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

export default function RTCWebcam(){
  Hunkify(this)

  let imgOut = this.output('ImageData', 'frame')

  let dom = this.document()
  let msg = $(`<div>retrieving video...</div>`).get(0)
  $(dom).append(msg)
  let video = $('<video>').get(0)
  let canvas = $('<canvas>').get(0) // invisible canvas 4 us

  let streaming = false
  let context = canvas.getContext('2d')

  this.init = () => {
    navigator.mediaDevices.getUserMedia({video: true, audio: false}).then((stream) => {
      $(dom).append(video)
      video.srcObject = stream
      video.play()
      video.addEventListener('canplay', (evt) => {
        $(msg).empty()
        if(!streaming){
          streaming = true
          canvas.width = video.videoWidth
          canvas.height = video.videoHeight
        }
        /*
        let capture = () => {
          context.drawImage(video, 0, 0, canvas.width, canvas.height)
          let imgData = context.getImageData(0, 0, canvas.width, canvas.height)
          console.log(imgData)
          setTimeout(capture, 2000)
        }
        setTimeout(capture, 2000)
        */
      })
    }).catch((err) => {
      $(msg).text('error retrieving webcam from system, see logs')
      console.error("Webcam Hunk couldn't retrieve a stream:", err)
    })
  }

  this.loop = () => {
    if(streaming && !imgOut.io()){
      // capture frames as imagedata, onto the virtual canvas,
      context.drawImage(video, 0, 0, canvas.width, canvas.height)
      // pull img data from that vcanvas -> the output,
      imgOut.put(context.getImageData(0,0, canvas.width, canvas.height))
    }
  }

}
