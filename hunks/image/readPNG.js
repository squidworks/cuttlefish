/*
hunks/image/readpng.js

gather PNG image from the aether

Jake Read at the Center for Bits and Atoms with Neil Gershenfeld and Leo McElroy
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import { Hunkify, Input, Output, State } from "../hunks.js";

export default function UploadPNG() {
  // this fn attaches handles to our function-object,
  Hunkify(this);

  let imgOut = this.output('ImageData', 'image')
  let dpiOut = this.output('number', 'dpi')
  let widthOut = this.output('number', 'width')
  let heightOut = this.output('number', 'height')

  let trig = this.state('boolean', 'release', false)
  trig.onChange = (value) => {
    imageUpdated = true
    dpiUpdated = true
    trig.set(false)
  }

  let dom = this.document()
  let canvas = $('<canvas>').get(0) // to draw, scaled-down,
  let vcanvas = $('<canvas>').get(0) // to store / manipulate off screen and full size
  let vcontext = vcanvas.getContext('2d')
  // local datas,
  let imageData = null
  let imageUpdated = false
  let idealWidth = 285
  // and dpi,
  let imgDpi = 72
  let dpiUpdated = false
  let whUpdated = false
  let width = 0
  let height = 0

  // write some startup DOM elements,
  let button = $('<input type="file" accept="image/png">').on('input', (evt) => {
    let file = evt.originalEvent.target.files[0]
    if (!file) return
    let imageReader = new FileReader()
    imageReader.onload = (load) => {
      let img = new Image()
      img.src = load.target.result
      img.onload = () => {
        vcanvas.width = img.width
        vcanvas.height = img.height
        vcontext.drawImage(img, 0, 0)
        // we can pull the nicely-formatted 'imageData' type from the context,
        imageData = vcontext.getImageData(0, 0, img.width, img.height)
        imageUpdated = true
        // *and* we can draw at our ideal width,
        let scale = idealWidth / img.width
        canvas.width = img.width * scale
        canvas.height = img.height * scale
        let context = canvas.getContext('2d')
        context.clearRect(0, 0, canvas.width, canvas.height)
        context.scale(scale, scale)
        context.drawImage(img, 0, 0)
        $(dom).append(canvas)
      }
    }
    imageReader.readAsDataURL(file)
    // we also want to read PNG headers, this is more painful.
    let rawReader = new FileReader()
    rawReader.onload = (bin) => {
      // s/o to Neil for this walker,
      var units = 0
      var ppx = 0
      var ppy = 0
      var buf = bin.target.result
      var view = new DataView(buf)
      var ptr = 8
      if (!((view.getUint8(1) == 80) && (view.getUint8(2) == 78) && (view.getUint8(3) == 71))) {
        console.warn("error: PNG header not found")
        return
      }
      while (1) {
        var length = view.getUint32(ptr)
        ptr += 4
        var type = String.fromCharCode(
          view.getUint8(ptr), view.getUint8(ptr + 1),
          view.getUint8(ptr + 2), view.getUint8(ptr + 3))
        ptr += 4
        if (type == "pHYs") {
          ppx = view.getUint32(ptr)
          ppy = view.getUint32(ptr + 4)
          units = view.getUint8(ptr + 8)
        }
        if (type == "IEND")
          break
        ptr += length + 4
      }
      if (units == 0) {
        console.warn("no PNG units not found, assuming 72 DPI")
        ppx = 72 * 1000 / 25.4
      }
      imgDpi = ppx * 25.4 / 1000
      dpiUpdated = true
      if (imageData) {
        console.log(imageData)
        // this ?
        width = (imageData.width / imgDpi) * 25.4
        height = (imageData.height / imgDpi) * 25.4
        whUpdated = true
        console.warn(`size in mm is w: ${width.toFixed(3)}, h: ${height.toFixed(3)}`)
      }
      console.warn('dpi is: ', imgDpi)
    }
    rawReader.readAsArrayBuffer(file)
  })

  $(dom).append(button)

  this.loop = () => {
    if (imageData && imageUpdated && !imgOut.io()) {
      imageUpdated = false
      imgOut.put(imageData)
    }
    if (imgDpi && dpiUpdated && !dpiOut.io()) {
      dpiUpdated = false
      dpiOut.put(imgDpi)
    }
    if (whUpdated && !widthOut.io() && !heightOut.io()) {
      whUpdated = false
      widthOut.put(width)
      heightOut.put(height)
    }
  }
}
