/*
hunks/image/displayImageData.js

render imageData into a canvas in the DOM

Jake Read at the Center for Bits and Atoms with Neil Gershenfeld and Leo McElroy
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import { Hunkify, Input, Output, State } from "../hunks.js";

export default function DisplayImageData() {
  Hunkify(this);

  let imageIn = this.input("ImageData", "image", this);

  let dom = this.document()
  let canvas = $('<canvas>').get(0)
  let context = canvas.getContext('2d')
  canvas.height = 100
  canvas.width = 100
  $(dom).append(canvas)
  let vcanvas = $('<canvas>').get(0)
  let vcontext = vcanvas.getContext('2d')
  // vcanvas,

  let canvasUid, descriptionUid;
  let idealWidth = 395;

  this.init = () => {
    //
  }


  this.loop = () => {
    if (imageIn.io()) {
      let img = imageIn.get();
      vcanvas.height = img.height
      vcanvas.width = img.width
      vcontext.putImageData(img, 0, 0)
      // and scale ... ?
      let scale = idealWidth / img.width
      canvas.width = img.width * scale
      canvas.height = img.height * scale
      //context.clearRect(0, 0, canvas.width, canvas.height)
      context.scale(scale, scale)
      context.drawImage(vcanvas, 0, 0)
      /*
      createImageBitmap(img).then((image) => {
        //context.clearRect(0, 0, canvas.width, canvas.height)
        context.scale(scale, scale)
        context.drawImage(image, 0, 0)
        rendering = false
      })
      */
    }
  };
}
