/*
hunks/image/svgToImageData.js

read svg, make image data.

Jake Read at the Center for Bits and Atoms
Neil Gershenfeld at the CBA
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

// ahn test string ...
let svgString = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"100.80402657534\" height=\"151.75086719281\" viewBox=\"0 0 0.0284491370844639 0.0428274680101977\" style=\"background:black\"><path d=\" M  0,0.0328274680101977 L  0,0 L  0.00842724217606665,0 L  0.00842724217606665,0.005 L  0.0134272421760667,0.005 L  0.0134272421760667,0 L  0.0184491370844639,0 L  0.0184491370844639,0.0017 L  0.0151272421760667,0.0017 L  0.0151272421760667,0.0067 L  0.00842724217606665,0.0067 L  0.00842724217606665,0.0328274680101977 L  0,0.0328274680101977\" style=\"fill:#ffffff;fill-rule:evenodd;\" transform=\"translate(0.005 0.0378274680101977), scale(1, -1)\" /></svg>"

export default function svgToImageData() {
  Hunkify(this)
  // 'string' 'input', get it?
  let strin = this.input('string', 'svg')
  let imgOut = this.output('ImageData', 'image')
  let width = this.output('number', 'width (mm)')
  let dpi = this.state('number', 'dpi', 300)
  // to manipulate full scale:
  let vcanvas = document.createElement('canvas')
  let haveImageUpdate = false

  // ... getsize, draw, fill ?
  let getSize = (svg) => {
    // where 'svg' is some string, we return width, height, units
    let i = svg.indexOf("width")
    if (i == -1) {
      return ({
        width: 1,
        height: 1,
        units: 90
      })
    } else {
      var i1 = svg.indexOf("\"", i + 1)
      var i2 = svg.indexOf("\"", i1 + 1)
      var width = svg.substring(i1 + 1, i2)
      i = svg.indexOf("height")
      i1 = svg.indexOf("\"", i + 1)
      i2 = svg.indexOf("\"", i1 + 1)
      var height = svg.substring(i1 + 1, i2)
      let ih = svg.indexOf("height")
      let units = 0
      if (width.indexOf("px") != -1) {
        width = width.slice(0, -2)
        height = height.slice(0, -2)
        units = 90
      } else if (width.indexOf("mm") != -1) {
        width = width.slice(0, -2)
        height = height.slice(0, -2)
        units = 25.4
      } else if (width.indexOf("cm") != -1) {
        width = width.slice(0, -2)
        height = height.slice(0, -2)
        units = 2.54
      } else if (width.indexOf("in") != -1) {
        width = width.slice(0, -2)
        height = height.slice(0, -2)
        units = 1
      } else {
        units = 90
      }
      return ({
        width: width,
        height: height,
        units: units
      })
    }
  }

  let loadImage = (svg, size) => {
    // btoa converts str. to base 64
    let src = "data:image/svg+xml;base64," + window.btoa(svg)
    let img = new Image()
    img.setAttribute('src', src)
    let width = size.width * dpi.value / size.units
    let height = size.height * dpi.value / size.units
    img.onload = () => {
      console.log('img2 loads', img)
      // new vcanvas always,
      vcanvas = document.createElement('canvas')
      vcanvas.width = width
      vcanvas.height = height
      let ctx = vcanvas.getContext('2d')
      ctx.drawImage(img, 0, 0, width, height)
      // now we are set,
      haveImageUpdate = true
    }
  }

  let test = this.state('boolean', 'test', false)
  test.onChange = (value) => {
    let size = getSize(svgString)
    console.log('img2 test size', size)
    loadImage(svgString, size)
    test.set(false)
  }

  this.loop = () => {
    if(strin.io() && !haveImageUpdate){
      let str = strin.get()
      let size = getSize(str)
      size.width = parseFloat(size.width).toString()
      size.height = parseFloat(size.height).toString()
      console.log('img2 size', size)
      loadImage(str, size)
    }
    if (haveImageUpdate && !imgOut.io() && !width.io()) {
      imgOut.put(vcanvas.getContext('2d').getImageData(0, 0, vcanvas.width, vcanvas.height))
      width.put(vcanvas.width * 25.4/dpi.value)
      haveImageUpdate = false
    }
  }
}
