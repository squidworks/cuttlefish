/*
hunks/statemachines/dex

purpose built statemachine for the https://gitlab.cba.mit.edu/jakeread/displacementexercise
aka DEX
aka king gripper
aka the big beefy boi
etc

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

/*

loadcell readings (for calibration)
0g: 25
100g: 14854
200g: 29649
300g: 44453
500g: 74061
700g: 103695

*/

// these are ES6 modules
import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

export default function DEX() {
  Hunkify(this)

  // to operate
  let motorOut = this.output('int32', 'motor output', this)
  // request a reading,
  let loadcellTrigger = this.output('boolean', 'loadcell trigger', this)
  // open loop displacement
  let cdOut = this.output('number', 'displacement (um)')

  // to config,
  let runState = this.state('boolean', 'running', false)
  let resetState = this.state('boolean', 'reset', false)

  let stepsToIncrement = (steps) => {
    // for this many steps,
    return steps / stepsPerMM.value
  }

  let incrementToSteps = (increment) => {
    return Math.ceil(increment * stepsPerMM.value)
  }

  // movement sizes info,
  let stepsPerMM = this.state('number', 'steps per mm', 4960)
  let incrementSize = this.state('number', 'increment (mm)', stepsToIncrement(100))

  // to run,
  let currentDisplacement = this.state('number', 'current displacement (mm)', 0)

  // delay,
  let stepDelay = this.state('number', 'delay(ms)', 500)

  resetState.onChange = (value) => {
    currentDisplacement.set(0)
    runState.set(false)
    resetState.set(false)
  }

  incrementSize.onChange = (value) => {
    // from <um> in fixed steps, do stepsPerUm / value
    incrementSize.set(incrementToSteps(value) / stepsPerMM.value)
  }

  this.init = () => {
    // uh-uuuuh
    runState.set(false)
  }

  let waiting = false
  let shipIt = () => {
    waiting = true
    motorOut.put(Math.round(stepsPerMM.value * incrementSize.value))
    setTimeout(() => {
      loadcellTrigger.put(true)
      currentDisplacement.set(currentDisplacement.value + incrementSize.value)
      cdOut.put(currentDisplacement.value)
      waiting = false
    }, stepDelay.value)
  }

  this.loop = () => {
    // if motor and loadcell trigger empty, release another round:
    if(runState.value){
      if(!loadcellTrigger.io() && !motorOut.io() && !cdOut.io() && !waiting){
        shipIt()
      }
    }
  }
}
