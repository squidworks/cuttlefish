/*
hunks/x_adhoc/center.js

find subpixel hotspot in grayscale image

Jake Read at the Center for Bits and Atoms with Neil Gershenfeld and Leo McElroy
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

import REG from '../../libs/regression.js'

export default function PolynomialFit() {
  Hunkify(this)

  let dataIn = this.input('reference', 'data')
  let predict = this.input('number', 'predict')

  let fitDisplay = this.output('reference', 'fit')
  let pout = this.output('number', 'predict') // thruput,
  let prediction = this.output('number', 'prediction')

  let expression = this.state('string', 'result', '-')

  this.init = () => {
    expression.set('-')
  }

  let fit = null

  this.loop = () => {
    if (dataIn.io()) {
      let datas = JSON.parse(JSON.stringify(dataIn.get()))
      fit = REG.polynomial(datas, { order: 3, precision: 6 })
      console.log(fit)
      expression.set(fit.string)
      // go-by to check, for x bounds of data, by .. 0.5 (arbitrary)
      let min = Infinity
      let max = -Infinity
      for (let i in datas) {
        if (datas[i][0] < min) min = datas[i][0]
        if (datas[i][0] > max) max = datas[i][0]
      }
      let fdata = []
      let count = 0
      for (let i = min; i < max; i += 0.2) {
        fdata.push(fit.predict(i))
      }
      if (!fitDisplay.io()) fitDisplay.put(JSON.parse(JSON.stringify(fdata)))
    }
    if (!fit) {
      if(predict.io()) predict.get()
    } else if (predict.io() && !prediction.io() && !pout.io()) {
      let res = fit.predict(predict.get())
      prediction.put(res[1])
      pout.put(res[0])
    }
  }
}
