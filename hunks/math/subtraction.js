/*
hunks/math/absolutevalue.js

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/


import {
  Hunkify,
  Input,
  Output,
  State
} from '../hunks.js'

export default function Subtraction(){
  Hunkify(this)

  let a = this.input('number', 'a')
  let b = this.input('number', 'b')
  let c = this.output('number', 'c')

  this.loop = () => {
    if(a.io() && b.io() && !c.io()){
      c.put(a.get() - b.get())
    }
  }
}
