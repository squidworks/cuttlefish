/*
core/scope.js

scope! check check, view display

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import cm from '../view/contextmenu.js'
import dt from '../view/domtools.js'
import blocks from '../view/blocks.js'

// scope is our UI tool: it draws *everything*
export default function Scope(wrapper, tlv) {
  // toplevel scroll-around / playa
  let plane = $('<div>').addClass('plane').attr('id', 'NROLPLANE').get(0)
  plane.handle = tlv // the dom->graph handle is to the view -> plane.handle.scope = this
  tlv.plane = plane
  let cs = 1 // our current scale
  $(plane).css('background', 'url("asset/bg.png")').css('width', '100px').css('height', '100px')
  $(wrapper).append(plane)
  // init transform of the plane,
  let dft = {
    s: cs,
    x: 0,
    y: 0,
    ox: 0,
    oy: 0
  }
  dt.writeTransform(plane, dft)
  dt.writeBackgroundTransform(wrapper, dft)

  // existence, but no checks yes,
  window.check = false

  /* ---------------------------    ---------------------------- */
  /* --------------------- USER LISTENERS ---------------------- */
  /* ---------------------------    ---------------------------- */

  wrapper.addEventListener('contextmenu', (evt) => {
    evt.preventDefault()
    evt.stopPropagation()
    if ($(evt.target).is('.defcore')) {
      cm.defMenu(evt)
    } else {
      let tcontext
      if ($(evt.target).is('#wrapper')) {
        tcontext = tlv
      } else if ($(evt.target).is('.subplane')) {
        tcontext = evt.target.handle
      }
      if (tcontext) {
        cm.planeMenu(evt, tcontext)
      } else {
        cm.errDisplay('no apparent context for this context-click', evt)
      }
    }
  })

  // zoom on wheel
  wrapper.addEventListener('wheel', (evt) => {
    evt.preventDefault()
    evt.stopPropagation()

    let ox = evt.clientX
    let oy = evt.clientY

    let ds
    if (evt.deltaY > 0) {
      ds = 0.025
    } else {
      ds = -0.025
    }

    let ct = dt.readTransform(plane)
    ct.s *= 1 + ds
    ct.x += (ct.x - ox) * ds
    ct.y += (ct.y - oy) * ds

    // max zoom pls thx
    if (ct.s > 1.5) ct.s = 1.5
    if (ct.s < 0.05) ct.s = 0.05
    cs = ct.s
    dt.writeTransform(plane, ct)
    dt.writeBackgroundTransform(wrapper, ct)

    // shouldn't have to redraw links, they should be rendered in the plane, but
    // we used to call this here: view.drawLinks()
  })

  // to pan, drag, (resize), etc
  wrapper.addEventListener('mousedown', (evt) => {
    if ($(evt.target).is('.defcore')) {
      evt.preventDefault()
      evt.stopPropagation()
      // if we drag the title, anything 'attached' according to rules
      // should follow
      let block = evt.target
      dt.dragTool((evt) => { // this just wraps up a global drag event, fires that on move, releases and deletes it on mouseup
        if (!block.handle) {
          console.error('was expecting this title to have a handle to ah def')
          // therefor is-not-subject to drag, therefor wyd, m8 ?
          return
        }
        block.handle.position.x += evt.movementX * (1 / cs)
        block.handle.position.y += evt.movementY * (1 / cs)
        blocks.repositionDef(block.handle)
      })
    } else if ($(evt.target).is('.output')) {
      evt.preventDefault()
      evt.stopPropagation()
      let opblock = evt.target
      if (!opblock.handle) {
        // handles should be routed at this point
        console.error('bad / no handle for this .output, wyd? - exiting')
        return
      }
      // have our output block, now seek floater
      let floater = $(`<div>${opblock.handle.type}</div>`).attr('id', 'floater').get(0)
      $(opblock.handle.parent.context.plane).append(floater)
      let position = dt.readTransform(opblock)
      position.x -= floater.clientWidth
      dt.writeTransform(floater, position)
      dt.dragTool((drag) => {
        position.x += drag.movementX * (1 / cs)
        position.y += drag.movementY * (1 / cs)
        dt.writeTransform(floater, position)
        dt.drawWire(opblock, floater)
      }, (up) => {
        // anything with a .handle is some object in our tree
        if (up.target.handle) {
          if (up.target.handle.is === "input") {
            let cnOp = opblock.handle
            let cnIp = up.target.handle
            cnOp.parent.context.buildRoute(cnOp, cnIp).then(() => {
              cm.successDisplay('route complete')
            }).catch((err) => {
              console.error(err)
              cm.errDisplay('trouble building this route')
            })
          } else {
            console.warn('op dragged to non-input')
          }
        }
        // in either case, we rm the floater
        dt.rmWire(opblock, floater)
        $(floater).remove()
      })
    } else if ($(evt.target).is('#wrapper') || $(evt.target).is('.subplane')) {
      evt.preventDefault()
      evt.stopPropagation()
      dt.dragTool((drag) => {
        drag.preventDefault()
        drag.stopPropagation()
        let ct = dt.readTransform(plane)
        ct.x += drag.movementX
        ct.y += drag.movementY
        dt.writeTransform(plane, ct)
        dt.writeBackgroundTransform(wrapper, ct)
      })
    } else if ($(evt.target).is('.resizehandle')) {
      evt.preventDefault()
      evt.stopPropagation()
      dt.dragTool((drag) => {
        evt.preventDefault()
        evt.stopPropagation()
        evt.target.handle.onresize({
          x: drag.movementX * (1 / cs),
          y: drag.movementY * (1 / cs),
        })
        dt.writeTransform(evt.target, {
          x: evt.target.handle.clientWidth - evt.target.clientWidth / 2,
          y: evt.target.handle.clientHeight - evt.target.clientHeight / 2
        })
      })
    } else {
      return
    }
  })

  // key listeners (are global, bc ...) KEYCODES
  document.addEventListener('keydown', (evt) => {
    // only op when evt not fired inside of something else
    if (!$(evt.target).is('body') && evt.keyCode !== 27) {
      return
    }
    // otherwise,
    if (evt.key === 'm') {
      // make ah menu,
      evt.preventDefault()
      evt.stopPropagation()
      cm.planeMenu(null, tlv)
    } else if (evt.key === 'z') {
      console.warn('keydown for zoom extents')
      evt.preventDefault()
      evt.stopPropagation()
      zoomExtents()
    } else if (evt.key === 'd') {
      console.warn('keydown for debug...')
      evt.preventDefault()
      evt.stopPropagation()
      // your debug land,
      // ...
      //writeMessage('addprogram', 'ntlink')
    } else if (evt.keyCode === 27) {
      // escapekey
      console.log('escape!')
      $('#wrapper').find('.contextmenu').remove()
      // also find floaters ...
    }
    return false
  })

  /* ---------------------------    ---------------------------- */
  /* ---------------------- DRAW / UPDATE ---------------------- */
  /* ---------------------------    ---------------------------- */

  // UIs have runtimes too,
  // here, we check if the view has changed anything,
  // and rerender any blocks that we would need to ...
  // in completion, we could go element-by-element to see if they have updates,
  // and update those blocks only
  // wherein setting something in the view with (setUpdate) or whatever bubbles up the tree

  let checkView = (view) => {
    // first off, if we wipe the whole thing,
    // we don't have to handle anything else
    if (view.hasCompleteRedraw) {
      console.log("SCOPE run for complete redraw")
      blocks.wipeContext(view)
      for (let i = 0; i < view.defs.length; i++) {
        blocks.redrawDef(view.defs[i])
        view.defs[i].hasUpdate = false
      }
      blocks.redrawContexWires(view)
      view.hasConnectivityChange = false
      view.hasDefUpdate = false
      view.hasCompleteRedraw = false
      return
    }

    // otherwise, we should first handle def updates
    // (this also counts new definitions)
    // eventually will do / switch this per ll item
    // defs w/ updates are for: state items, name changes,
    // and probably connection / rewires (add / sub inputs)
    if (view.hasDefUpdate) {
      console.log('SCOPE run for def update')
      for (let i = 0; i < view.defs.length; i++) {
        if (view.defs[i].hasUpdate) {
          // for now, we just redraw the entire definition, whenever we have updates for it
          blocks.redrawDef(view.defs[i])
          // ah yeah, so we also occasionally load defs in here w/o having their link-drawing
          // so we should just draw links from outputs as well as from inputs, then
          // want a clean way to do that next...
          view.defs[i].hasUpdate = false
        }
      }
      view.hasDefUpdate = false
    }

    // also, we do this globally. tough. scrape and move on
    if (view.hasConnectivityChange) {
      console.log('SCOPE run for connectivity change')
      blocks.redrawContexWires(view)
      view.hasConnectivityChange = false
    }

    if (view.hasDefPositionUpdate) {
      console.log('SCOPE run for reposition from view')
      for (let i = 0; i < view.defs.length; i++) {
        if (view.defs[i].hasPositionUpdate) {
          blocks.repositionDef(view.defs[i])
          view.defs[i].hasPositionUpdate = false
        }
      }
      view.hasDefPositionUpdate = false
    }
  }

  let open = (context) => {
    // new plane every time!
    // handle to our context's container (the link)
    let ldef = context.exterior()
    // this can happen, when i.e. upstairs is resetting / refreshing ... ?
    if (!ldef) return //throw new Error("no apparent link container for this context")
    // things should now be redrawn,
    blocks.redrawDef(ldef)
  }

  let close = (context) => {
    // ... need this,
    if (!context) return
    // close downstream, this will recurse,
    for (let def of context.defs) {
      if (def.type == 'link') {
        close(def.interior()) // here why we wrote line 2 of this, sometimes within returns n o t h i n g
      }
    }
    // that-which-is-wrapped-around-us
    let ldef = context.exterior()
    if (!ldef) return // throw new Error("no apparent link container for this context, close meaningless, que?")
    // ok, cleanup the context,
    blocks.wipeContext(context)
    // she gone, burn the letters
    $(context.plane).remove()
    delete context.plane
    // plenty of fish,
    blocks.redrawDef(ldef)
  }

  this.check = () => {
    if(window.check){
      try {
        // check levels recursively,
        let recursor = (context) => {
          checkView(context)
          for (let def of context.defs) {
            if (def.type == 'link') {
              let interior = def.interior()
              if (interior) {
                let visible = $.contains(document, interior.plane)
                if (interior.open && !visible) {
                  open(interior)
                }
                if (!interior.open && visible) {
                  close(interior)
                }
                if ($.contains(document, interior.plane)) { // have to check again,
                  recursor(interior)
                }
              }
            }
          }
        }
        recursor(tlv)
        window.check = false
      } catch (err) {
        console.error(err)
        window.halt()
      }
    }
  }
}
