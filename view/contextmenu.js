/*
view/vcontextmenu.js

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

// writes the context (right-click) menu ...

import dt from './domtools.js'
import blocks from './blocks.js'
import gg from '../gogetter.js'

let findOrBuild = (evt) => {
  // ok, we either find this thing, or build a new one and place it in plane
  // according to where the evt was fired from,
  // if no event, top level plane,
  // either find the menu, or make a new one at evt location (in wrapper), and
  // write some text
  let menu = $('#wrapper').find('.contextmenu').get(0)
  if (!menu) {
    let plane = {}
    let pos = {}
    if (evt) {
      if ($(evt.target).is("#wrapper")) {
        plane = window.tlv.plane
        let pt = dt.readTransform(plane)
        pos.s = 1 + ((1 / pt.s) - 1) * 0.5
        pos.x = (evt.layerX - pt.x) * (1 / pt.s)
        pos.y = (evt.layerY - pt.y) * (1 / pt.s)
      } else {
        if (!$(evt.target).is('.subplane')) return
        plane = evt.target
        pos.x = evt.layerX + 3
        pos.y = evt.layerY - 29
        pos.s = 1
      }
    } else {
      // no event, use default plane,
      plane = $('#wrapper').get(0)
      pos = { x: 250, y: 250, s: 1 }
    }
    menu = $('<div>').addClass('contextmenu').get(0)
    dt.writeTransform(menu, pos)
    $(plane).append(menu)
  }
  menu.addEventListener('wheel', (evt) => {
    let mt = dt.readTransform(menu)
    dt.writeTransform(menu, {
      s: mt.s,
      x: mt.x,
      y: mt.y - evt.deltaY * 4
    })
    evt.preventDefault()
    evt.stopPropagation()
  })
  return menu
}

let errDisplay = (msg, evt) => {
  let menu = findOrBuild(evt)
  // wipe it,
  $(menu).children().remove()
  $(menu).css('background-color', 'red')
  let title = $(`<div>err</div>`).addClass('contextTitle').get(0)
  $(title).text('error')
  $(title).append(`<p>${msg}</p>`)
  $(menu).append(title)
  fadeOut(1500)
}

let successDisplay = (msg, evt) => {
  let menu = findOrBuild(evt)
  // wipe it,
  $(menu).children().remove()
  $(menu).css('background-color', 'green')
  let title = $(`<div>ok</div>`).addClass('contextTitle').get(0)
  $(title).text('success')
  $(title).append(`<p>${msg}</p>`)
  $(menu).append(title)
  fadeOut(500)
}

let addContextTitle = (text) => {
  $('#wrapper').find('.contextmenu').get(0).append($('<ul>' + text + '/</li>').get(0))
}

let addContextOption = (text, click) => {
  $('#wrapper').find('.contextmenu').get(0).append($('<li>' + text + '</li>').click((click)).get(0))
}

// this thing also writes the text-to-select code
let changeContextTitle = (text) => {
  // clear,
  let menu = $('#wrapper').find('.contextmenu').get(0)
  $(menu).children().remove()
  // overkill, but fun
  let title = $(`<div>${text}</div>`).addClass('contextTitle').get(0)
  $(menu).append(title)
  //   // add the text window, select it ...
  // also want: keyboard and enter ... track which is selected, then ?
  let tinput = $('<input>').attr('type', 'text').attr('size', 28).attr('value', '').get(0)
  $(title).append(tinput)
  $(tinput).focus()
  $(tinput).select()
  tinput.addEventListener('keyup', (evt) => {
    let arr = $(menu).children('li').toArray()
    if (evt.keyCode == 13) {
      // enter case
      let pick = $(menu).children('.highlighted').get(0)
      if (pick) {
        pick.click()
      }
    } else {
      // key character ?
      let filts = tinput.value.toUpperCase().split(' ')
      let sel = false
      // separate items by space, search for either,
      // reorder by search quality
      for (let item of arr) {
        // we want the items that match *some part of both items in the filt*
        let pass = true
        for (let conf of filts) {
          if ($(item).text().toUpperCase().indexOf(conf) < 0) {
            pass = false
          }
        }
        if (pass) {
          item.style.display = ""
          // mark as selected, then this is our enter-docu
          if (!sel) {
            $(item).addClass('highlighted')
            sel = true
          } else {
            $(item).removeClass('highlighted')
          }
        } else {
          item.style.display = "none"
          $(item).removeClass('highlighted')
        }
      }
    }
    return
  })
}

let fadeOut = (ms) => {
  setTimeout(() => {
    $('#wrapper').find('.contextmenu').fadeOut(ms, function() {
      $(this).remove()
    })
  }, ms)
}

let setupForSave = (se, callback) => {
  $(se.target).closest('li').text('')
  let tinput = $('<input>').attr('type', 'text').attr('size', 24).attr('value', 'name').get(0)
  $(se.target).closest('li').append(tinput) // etc
  $(tinput).focus()
  $(tinput).select()
  $(tinput).on('keyup', (ke) => {
    console.log('save key')
    if (ke.keyCode == 13) {
      console.log('keycode 13')
      callback(tinput.value)
    }
  })
}

function defMenu(evt) {
  let def = evt.target.handle
  let context = def.context
  let plane = def.context.plane
  $('#wrapper').find('.contextmenu').remove()
  let menu = $('<div>').addClass('contextmenu').get(0)
  let ps = dt.readTransform(evt.target)
  let pt = dt.readTransform(plane)
  // write in, then write to
  $(plane).append(menu)

  // this is kind of odd, but conversational
  changeContextTitle('do:   ')

  /*
  addContextOption('<i class="em em-abc"></i> rename hunk', (evt) =>{
    $(evt.target).text('')
    let tinput = $('<input>').attr('type', 'text').attr('size', 24).attr('value', 'new name').get(0)
    $(evt.target).append(tinput)
    $(tinput).focus()
    $(tinput).select()
    $(tinput).on('keyup', (evt) => {
      if (evt.keyCode == 13) {
        context.requestRenameHunk(this.ind, tinput.value).then((def) => {
          console.log('cleared new name for', def)
          $(menu).remove()
        })
      }
    })
  })
  */

  addContextOption('<i class="em em-repeat_one"></i> copy hunk', (evt) => {
    context.requestAddHunk(def.type).then((hnk) => {
      $(menu).remove()
    }).catch((err) => {
      errDisplay('err adding another one of these... consoles ->')
    })
  })

  addContextOption('<i class="em em-coffin"></i> remove hunk', (evt) => {
    context.requestRemoveHunk(def.ind).then(() => {
      $(menu).remove()
    }).catch((err) => {
      errDisplay("couldn't seem to rm this thing...")
    })
  })

  addContextOption('<i class="em em-arrows_counterclockwise"></i> reload from source', (evt) => {
    context.reloadHunk(def.ind).then(() => {
      $(menu).remove()
    }).catch((err) => {
      console.error(err)
      errDisplay("err while reloading...")
    })
  })

  // ok, ps is the position, in the plane, of the item we clicked on
  // pt is the transform of the plane
  dt.writeTransform(menu, {
    s: 1, //+ ((1 / pt.s) - 1) * 0.5,
    x: ps.x,
    y: ps.y - menu.clientHeight - 5 // menu.clientHeight * (1 + ((1 / pt.s) - 1) * 0.5) - 5
  })
}

// sometimes called w/o mouse event
function planeMenu(evt, context) {
  // position from the evt is in wrapper-space (screen space)
  // but we are interested in this as well,
  let menu = findOrBuild(evt)

  // hmmm
  changeContextTitle('do:  ')

  /* ---------------------------    ---------------------------- */
  /* --------------- LOCAL OPTIONS, for CONTEXT ---------------- */
  /* ---------------------------    ---------------------------- */

  addContextOption(`<i class="em em-twisted_rightwards_arrows"></i> refresh the view`, (ce) => {
    // this actually gets wiped when the first hunk arrives, so
    $(ce.target).closest('li').text('refreshing...')
    context.refresh().then(() => {
      successDisplay('refresh ok')
    })
  })

  addContextOption(`<i class="em em-wave"></i> say hello`, (ce) => {
    $(ce.target).closest('li').text(`view says hello ...`)
    context.sayHelloToManager().then((ms) => {
      successDisplay(`msg took ${ms}ms rtt from browser`)
    })
  })

  // req a new hunk,
  addContextOption('<i class="em em-construction_worker"></i> add a hunk', (ce) => {
    $(ce.target).closest('li').text('requesting a list of hunks...')
    context.requestListAvail().then((stringlist) => {
      //console.log('list', stringlist)
      changeContextTitle('load:')
      // sort the list...
      let treelike = [{
        parentname: '',
        list: []
      }]
      for (let item of stringlist) {
        let slash = item.indexOf('/')
        if (slash > 0) {
          let head = item.substring(0, slash)
          // add to or make,
          let parent = treelike.find((cand) => {
            return cand.parentname === head
          })
          if (parent) {
            parent.list.push(item)
          } else {
            treelike.push({
              parentname: head,
              list: [item]
            })
          }
        } else {
          treelike[0].list.push(item)
        }
      }
      for (let branch of treelike) {
        addContextTitle(branch.parentname)
        for (let item of branch.list) {
          addContextOption(item, (ce) => {
            $(ce.target).closest('li').append(' > requested ... ')
            context.requestAddHunk(item).then((def) => {
              let menu = $(context.plane).children('.contextmenu').get(0)
              if (menu) {
                let mp = dt.readTransform(menu)
                def.position = { x: mp.x, y: mp.y }
              } else {
                def.position = { x: 250, y: 250 }
              }
              if(def.blocks) blocks.repositionDef(def)
              successDisplay('hunk alive!')
              //console.log('one hunk as promised', def)
            }).catch((err) => {
              console.log('err', err)
              errDisplay('a bad time was had, see consoles')
            })
          })
        }
      }
    })
  })

  /* ---------------------------    ---------------------------- */
  /* ------------------ QUEEN OF THE MOUNTAIN ------------------ */
  /* ---------------------------    ---------------------------- */

  // write these if the click was into the toplevel
  if (context.name !== 'tlview') return false
  // save the entire system,
  addContextOption('<i class="em em-books"></i> save this system', (ce) => {
    // for posterity, bc of often-broken-due-to-def-rewrite (bugfarm!),
    // we run a GO before we do this...
    let debug = true
    if (debug) console.log('begin save...')
    // how about: .save() writes an object-type serializable thing
    // to extend, in the future we can do .save[ind_0, ind_1, ...],
    // that lists indices of hunks we want to include in this save,
    // so we can pull out sub-patches etc
    context.save().then((patch) => {
      console.log('ptch', patch)
      // now we should be ok to ship this home ...
      setupForSave(ce, (name) => {
        console.log('save return, posting...')
        jQuery.post(`/save/systems/${name}`, patch, (res) => {
          if (!res.success) {
            changeContextTitle('error while saving, see logs')
            console.error("server-side error while saving")
            console.error(res)
          } else {
            successDisplay(`saved to /save/systems/${name}`)
          }
        }, 'json')
      })
    }).catch((err) => {
      changeContextTitle('error while saving, see logs')
      console.error('err during patch save')
      console.error(err)
    })
  }) // end save entire system

  // MERGE A SYSTEM
  // careful: two 'ce' variables in lower context ?
  addContextOption('<i class="em em-hammer_and_wrench"></i> restore a system', (ce) => {
    // get the data ...
    gg.recursivePathSearch('save/systems/').then((stringlist) => {
      //console.log('list', stringlist)
      changeContextTitle('load:')
      // sort the list...
      let treelike = [{
        parentname: '',
        list: []
      }]
      for (let item of stringlist) {
        let slash = item.indexOf('/')
        if (slash > 0) {
          let head = item.substring(0, slash)
          // add to or make,
          let parent = treelike.find((cand) => {
            return cand.parentname === head
          })
          if (parent) {
            parent.list.push(item)
          } else {
            treelike.push({
              parentname: head,
              list: [item]
            })
          }
        } else {
          treelike[0].list.push(item)
        }
      }
      for (let branch of treelike) {
        addContextTitle(branch.parentname)
        for (let item of branch.list) {
          addContextOption(item, (ce) => {
            $(ce.target).closest('li').append(' > requested ... ')
            gg.getJson(`save/systems/${item}.json`).then((obj) => {
              // apparently, it's actually promises all the way down
              // 2nd arg is debug state
              context.reinstate(obj).then((yikes) => {
                // trigger re-render? do we get heavy handed here and shut it down
                // ... i.e. don't render anything until it's thru ?
                successDisplay('system restore success')
              })
            }).catch((err) => {
              errDisplay("couldn't retrieve this patch, see consoles")
              console.error(err)
            })
          })
        }
      }
    }).catch((err) => {
      errDisplay("couldn't retrieve a list of systems, see consoles")
      console.error(err)
    })
  })

}

export default {
  planeMenu,
  defMenu,
  errDisplay,
  successDisplay
}
