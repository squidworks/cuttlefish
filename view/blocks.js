/* view/blocks.js

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability. */

import { isIntType } from '../typeset.js'

import dt from './domtools.js'

// adds some helper fn's to blocks, so that we can quickly append children to them
let makeBlockParent = (bl) => {
  bl.blockChildren = []
  bl.take = (block) => {
    bl.blockChildren.push(block)
    block.blockParent = bl
  }
  bl.remove = (block) => {
    let bi = bl.blockChildren.findIndex((cand) => {
      return cand === block
    })
    if (bi >= 0) {
      bl.blockChildren.splice(bi, 1)
    } else {
      console.error('missing child block when trying to remove, shruggy')
    }
  }
  return bl
}

let getBlockStackHeight = (blocks) => {
  let y = 0
  for (let bl of blocks) {
    if ($(bl).is('.state') || $(bl).is('.defcore')) y += bl.clientHeight + 5
  }
  return y
}

// stack: just state,
let getBlockStackWidth = (blocks) => {
  let x = 0
  for (let bl of blocks) {
    if ($(bl).is('.state') || $(bl).is('.defcore') || $(bl).is('.subplane')) {
      if (x < bl.clientWidth) x = bl.clientWidth
    }
  }
  return (x += 5)
}

let getBlockStackRightWidth = (blocks) => {
  let x = getBlockStackWidth(blocks)
  let y = 0
  for (let bl of blocks) {
    if ($(bl).is('.output')) {
      if (y < bl.clientWidth) y = bl.clientWidth
    }
  }
  return (x + y + 5)
}

let getBlockStackLeftWidth = (blocks) => {
  let x = 0
  for (let bl of blocks) {
    if ($(bl).is('.input')) {
      if (x < bl.clientWidth) x = bl.clientWidth
    }
  }
  return (x += 5)
}

/* ---------------------------    ---------------------------- */
/* ----------------------- LINK BLOCK ------------------------ */
/* ---------------------------    ---------------------------- */

/*

this one's a behemoth, as we do normal block stuff and/also
we bootstrap views-for-ll-things, buttons to transition those states, etc

*/

// button,
let squid = (ldef, titleblock) => {
  ldef.squid = $('<div>').addClass('loosebutton').addClass('block').get(0)
  ldef.squid.position = () => {
    return { x: 0, y: -ldef.squid.clientHeight - 5 }
  }
  ldef.squid.handle = ldef
  titleblock.take(ldef.squid)
  ldef.blocks.push(ldef.squid)
}

// oboy, links draw like oddballs,
let makeLink = (ldef, titleblock) => {
  // first we want to get some state for the link,
  // assuming that other levels will rebuild this when those
  // change (scope.js)
  let context = ldef.interior()
  if(context){
    // we are hooked to a view,
    if(context.open){
      // that view should be rendered,
      // has an interior context to render, and it's set to render,
      // build the plane, carry it around.
      // make the new plane,
      context.plane = $('<div>').addClass('subplane').attr('id', `${ldef.name}_plane`).get(0)
      // zerbras
      if(!$(ldef.context.plane).is('.offwhite')) $(context.plane).addClass('offwhite')
      if (!context.size){
        context.size = { x: 900, y: 600 }
      }
      dt.writeContainerSize(context.plane, context.size)
      // handle resize,
      // existence: to the plane where the link lives, append this plane,
      $(ldef.context.plane).append(context.plane)
      dt.addResizeHandle(context.plane) // handle, and further handler for onresize
      context.plane.onresize = (deltas) => {
        let size = dt.readContainerSize(context.plane)
        size.x += deltas.x
        size.y += deltas.y
        dt.writeContainerSize(context.plane, size)
        context.size = size
        // reposition the def's blocks - right side moves,
        repositionDef(ldef)
        // reposition all defs in the downstream: we might be squishing them about:
        for (let dsdef of context.defs) {
          repositionDef(dsdef)
        }
      } // end onresize,
      // add the context as a block that the link carries about:
      $(context.plane).addClass('block')
      context.plane.handle = context
      context.plane.position = () => {
        return { x: 0, y: getBlockStackHeight(ldef.blocks) }
      }
      ldef.blocks.push(context.plane)
      titleblock.take(context.plane)
      // add ah button to contract,
      squid(ldef, titleblock)
      $(ldef.squid).html('<i class="em em-shell"></i> collapse link')
      $(ldef.squid).one('click', (evt) => {
        context.open = false
        window.check = true
      })
      // redrawing here, so redraw them:
      context.setCompleteRedraw()
      // and we need to touch up our input / output positions,
      for(let op of ldef.outputs){
        op.block.position = () => {
          return {
            x: getBlockStackWidth(ldef.blocks),
            y: getBlockStackHeight(ldef.blocks) + (op.block.clientHeight + 5) * op.ind
          }
        }
      } //
      for(let ip of ldef.inputs){
        ip.block.position = () => {
          return {
            x: -ip.block.clientWidth - 5,
            y: getBlockStackHeight(ldef.blocks) + (ip.block.clientHeight + 5) * ip.ind
          }
        }
      }
    } else {
      // that view should not be rendered, act normal,
      squid(ldef, titleblock)
      $(ldef.squid).html('<i class="em em-squid"></i> open link')
      $(ldef.squid).one('click', (evt) => {
        context.open = true
        window.check = true
      })
    }
  } else if (ldef.upstream()){
    // we are the downstream link, the rx to your tx,
    titleblock.position = () => {
      let y = 0;
      for (let opd of ldef.outputs) {
        y += opd.block.clientHeight + 5
      }
      return { x: 0, y: y }
    }
    for(let op of ldef.outputs){
      op.block.position = () => {
        return {
          x: 0,
          y: -titleblock.position().y + (op.block.clientHeight + 5) * op.ind
        }
      }
    } //
    for(let ip of ldef.inputs){
      ip.block.position = () => {
        return {
          x: ldef.context.plane.clientWidth - ip.block.clientWidth,
          y: -titleblock.position().y + (ip.block.clientHeight + 5) * ip.ind
        }
      }
    }
  } else {
    console.warn(`std link in ${ldef.context.name} ${ldef.name}`)
    // nothing special, just another def. needs help to open,
    squid(ldef, titleblock)
    $(ldef.squid).html('<i class="em em-octopus"></i> build link context')
    $(ldef.squid).one('click', (evt) => {
      ldef.context.buildLinkContext(ldef).then((context) => {
        context.open = true
        window.check = true
      })
    })
  }
}

// typed, awkward
let makeState = (st, bl) => {
  // st is the state definition
  // we also need to store a local-copy-of-old state:
  st.lastValue = st.value
  // bl is the block we'll put all of our html codes in
  let title = $(`<div>${st.name} (${st.type})</div>`).addClass('stateheader').get(0)
  let input = {}
  let context = st.parent.context
  // truly, ideally, this would be integrated with typeset.js ...
  switch (typeof st.value) {
    case 'string':
      input = $('<input>').attr('type', 'text').attr('size', 32).attr('value', st.value)
        .css('float', 'right').css('width', '95%').get(0)
      st.reset = () => {
        input.value = st.value
        st.lastValue = st.value
      }
      input.addEventListener('change', (evt) => {
        context.requestStateChange(st, input.value).then(() => {
          // view sets w/ reset
        }).catch((err) => {
          console.error('bad state swap')
          input.value = st.lastValue
        })
        // new state: http://shrugguy.com
        input.value = '... requested ...'
      })
      $(bl).append(title, input)
      break // end 'string'
    case 'number':
      input = $('<input>').attr('type', 'text').attr('size', 24).attr('value', 'startup')
        .css('float', 'right').css('width', '100px').get(0)
      st.reset = () => {
        let cand = st.value.toString()
        if (cand.length > 20) {
          cand = parseFloat(cand).toFixed(6)
        }
        input.value = cand
        input.lastValue = st.value
      }
      st.reset() // since we have this nice fn for it, do this now
      input.addEventListener('change', (evt) => {
        let req
        if (isIntType(input.type)) {
          req = parseInt(input.value)
        } else {
          req = parseFloat(input.value)
        }
        context.requestStateChange(st, req).then(() => {
          // view sets w/ reset
        }).catch(() => {
          console.error('bad state swap')
          input.value = st.lastValue
        })
        input.value = '... requested ...'
      })
      $(title).append(input)
      $(bl).append(title)
      break
    case 'boolean':
      let span = $('<span style="float:right;"></span>').addClass('booleanspan').get(0)
      st.reset = () => {
        $(span).text(st.value.toString())
        st.lastValue = st.value
        if (st.value) {
          $(span).css('background-color', 'green')
        } else {
          $(span).css('background-color', 'red')
        }
      }
      st.reset()
      span.addEventListener('click', (evt) => {
        let req = null
        if ($(span).text() == 'true') {
          req = false
        } else if ($(span).text() == 'false') {
          req = true
        } else {
          //... waiting, probably says '... requested ...'
        }
        if (req != null) {
          context.requestStateChange(st, req).catch((err) => {
            console.error('state change error', err)
            $(span).text('error during request!')
          })
        }
        $(span).text('... requested ...')
        $(span).css('background-color', 'grey')
      })
      $(title).append(span)
      $(bl).append(title)
      break
    default:
      console.error('unaccounted for type while drawing state UI elements', st)
      console.error(`typeof finds ${typeof st.value}`)
      break
  }
}

let wipeWires = (context) => {
  // we'll clean up all of the wires attached to blocks,
  // by our context defintions
  // this will also clean-up that blocks' .wires array of refs,
  for (let df of context.defs) {
    if (df.blocks) {
      for (let block of df.blocks) {
        if (block.handle.is == 'output' || block.handle.is == 'input') {
          dt.rmAllWires(block)
        }
      }
    }
  }
  // since some of those defs / having wires / may have been deleted,
  // we also need to rm any strays.
  $(context.plane).children('.wire').remove()
}

// this will scrape the def, removing any of its div elements, etc
let cleanDef = (def) => {
  // keep this local for holdover,
  let ps = def.position
  // rules for us: only ever attach blocks to
  // things they have .handles to, so we can do:
  if (def.blocks) {
    for (let bl of def.blocks) {
      let hd = bl.handle
      if (hd.is == 'output') {
        dt.rmAllWires(hd.block)
        // this should get rid of the def's reference to it,
        delete hd.block
        // however, links are still a mess.
      } else if (hd.is == 'input'){
        // trickier for wires, due to their living w/ the output
        for(let op of hd.conn){
          dt.rmWire(op.block, hd.block)
        }
      }
      // don't wipe .hunks,
      if ($(bl).is('.block') && !$(bl).is('.subplane') && !$(bl).is('.hunk')) $(bl).remove()
    }
    // rasa, courtesy of the boss
    def.context.cleanDef(def)
    // reattaching position,
    if (ps) def.position = ps
  }
}

// this will completely un-draw any given context,
let wipeContext = (context) => {
  // rm all wires,
  wipeWires(context)
  // rm all contexts,
  for (let df of context.defs) {
    cleanDef(df)
  }
  // strays,
  $(context.plane).find('.block').not('.hunk').remove()
  // wires,
  $(context.plane).find('.wire').remove()
}

// this takes our raw-af defs and builds / rebuilds them into the objecst we want
// to drop in the dom / use in the UI
// two trees
let rebuildDef = (def, position) => {
  // wipe to zero,
  cleanDef(def)

  // need to make sure we have some position information before we draw
  if (!def.position) {
    if (position) {
      def.position = { x: position.x, y: position.y }
    } else {
      def.position = { x: 350, y: 150 }
    }
  }

  // we'll keep *all* of the blocks that belong to the definition in this handle:
  def.blocks = []

  // now we have also a graph of block elements...
  // blocks shold be actual elements, yeah?
  let titleblock = $(`<div>${def.name}</div>`).addClass('defcore').addClass('block').attr('id', `${def.name}_${def.ind}`).get(0)
  titleblock.position = () => {
    return { x: def.position.x, y: def.position.y }
  }
  makeBlockParent(titleblock)
  titleblock.handle = def
  def.block = titleblock
  def.blocks.push(titleblock)

  // build blocks for input objects,
  for (let i = 0; i < def.inputs.length; i++) {
    let inputdef = def.inputs[i]
    let inputblock = $(`<li>${inputdef.name} (${inputdef.type})</li>`).addClass('input').addClass('block').get(0)
    inputblock.handle = inputdef
    inputblock.position = () => {
      return {
        x: -inputblock.clientWidth - 5,
        y: (inputblock.clientHeight + 5) * i
      }
    }
    inputdef.block = inputblock
    titleblock.take(inputblock)
    def.blocks.push(inputblock)
  }

  // build blocks for output objects,
  for (let o = 0; o < def.outputs.length; o++) {
    let outdef = def.outputs[o]
    let outblock = $(`<li>(${outdef.type}) ${outdef.name}</li>`).addClass('output').addClass('block').get(0)
    outblock.position = () => {
      return {
        x: titleblock.clientWidth + 5,
        y: (outblock.clientHeight + 5) * o
      }
    }
    outblock.handle = outdef
    outdef.block = outblock
    titleblock.take(outblock)
    def.blocks.push(outblock)
  }

  // build blocks for state objects,
  for (let s = 0; s < def.states.length; s++) {
    let statedef = def.states[s]
    let stateblock = $('<div>').addClass('state').addClass('block').get(0)
    makeState(statedef, stateblock)
    stateblock.position = () => {
      let x = 0;
      let y = 0;
      y += titleblock.clientHeight + 5;
      for (let sb = 0; sb < stateblock.handle.ind; sb++) {
        y += stateblock.handle.parent.states[sb].block.clientHeight + 5
      }
      return { x: x, y: y }
    }
    titleblock.take(stateblock)
    stateblock.handle = statedef
    statedef.block = stateblock
    def.blocks.push(stateblock)
  }

  // find yourself (the native hunk element) in t h e g l o b a l d o m a i n
  if (def.hunk) {
    // this means that (1) the def is for a *genuine* hunk in the browser,
    // not a remote definition of one,
    // if it has a .dom element, that will be .hunk, find it and attach it
    // to the thing we're drawing
    // claim dom elements, global search
    let native = $(def.context.plane).find('.hunk').toArray().find((cand) => {
      return cand.hunk === def.hunk
    })
    if (native) {
      // issa block,
      $(native).addClass('block')
      native.handle = def
      // ok, I think I want to position this above everything else,
      native.position = () => {
        // this is more difficult, as we are looking @ the other blocks,
        return { x: 0, y: getBlockStackHeight(def.blocks) }
      }
      // attach to title,
      titleblock.take(native)
      def.blocks.push(native)
    }
  }

  // messy !
  if (def.type == 'link') makeLink(def, titleblock)
}

// redraw all links connected to this block: inputs / outputs
let drawBlockLinks = (block) => {
  // only op on blocks w/ connections
  if (!(block.handle.is == 'output' || block.handle.is == 'input')) return
  // find the view object,
  let context = block.handle.parent.context
  if (!context) {
    console.warn('no handle to top level view in this dom scope, wyd?')
    return
  }
  // do output links, if we have...
  if (block.handle.is == 'output') {
    for (let ip of block.handle.conn) {
      if (ip.block) dt.drawWire(block, ip.block)
    }
  } else if (block.handle.is == 'input') {
    for (let op of block.handle.conn) {
      if (op.block) dt.drawWire(op.block, block)
    }
  }
}

// blocks that (1) have no children or (2) have no parents will not be placed
let repositionBlock = (block) => {
  if (block.blockChildren) {
    dt.writeTransform(block, block.position())
    drawBlockLinks(block)
    for (let child of block.blockChildren) {
      try {
        dt.writeTransform(child, {
          x: block.position().x + child.position().x,
          y: block.position().y + child.position().y
        })
        drawBlockLinks(child)
      } catch (err) {
        console.log(child)
        console.error(err)
      }
    }
  } else {
    // we only place by children atm, this feels like it has high likelihood of messing
    // someone up in the future: apologies if that is the case
  }
}

let repositionDef = (def) => {
  // context bounds ...
  if (!def.context.plane) {
    // not completely unlikely, invisible views refreshing, perhaps... but those
    // should come thru ah reposition check-switch, not here direct, so
    console.warn('reposition for invisible def, wyd?')
    return
  }
  // check that we don't move out-of-bounds of our container,
  // if there is some exterior() link, we are being drawn in a subplane,
  if (def.context.exterior()) {
    let bw = def.context.plane.clientWidth
    let bh = def.context.plane.clientHeight
    // clip to bounds, plus some margin...
    let wr = getBlockStackRightWidth(def.blocks)
    let wl = getBlockStackLeftWidth(def.blocks)
    let h = getBlockStackHeight(def.blocks)
    // cover bottom left:
    if (def.position.x + wr > bw) def.position.x = bw - wr
    if (def.position.y + h > bh) def.position.y = bh - h
    if (def.position.x - wl < 0) def.position.x = wl
    if (def.position.y < 0) def.position.y = 0
  }
  // arrange,
  if (def.blocks) {
    for (let bl of def.blocks) {
      repositionBlock(bl)
    }
  }
}

let redrawDef = (def) => {
  //console.log('BLOCKS REDRAW for', def.name)
  // this wipes and redraws,
  rebuildDef(def)
  // place them all in the context,
  for (let bl of def.blocks) {
    $(def.context.plane).append(bl)
  }
  // arrange (should draw links as well)
  repositionDef(def)
  repositionDef(def) // don't ask me why... something about DOM element size recalculations
}

let redrawContexWires = (context) => {
  // since we may have rm'd some links, we wipe and restart
  wipeWires(context)
  for (let df of context.defs) {
    for (let bl of df.blocks) {
      // since we're going from zero, and not repositioning,
      // it's safe to just draw outputs, this saves 1/2 time
      if (bl.handle.is == 'output') drawBlockLinks(bl)
    }
  }
}

export default {
  redrawDef, // to add / update anything
  repositionDef, // to move anything
  repositionBlock, // to move by block (dragging children along)
  getBlockStackHeight, // total height of states, title
  getBlockStackWidth, // states width
  getBlockStackLeftWidth, // total left
  getBlockStackRightWidth, // total right
  redrawContexWires, // when context link topology changes
  wipeContext // to shutdown / cleanup
}
