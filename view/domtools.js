/*
view/vdom.js

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

// ay

// a utility to write a numbers-ordered transform object into the dom,
// tf: transform object
let writeTransform = (div, tf) => {
  //console.log('vname, div, tf', view.name, div, tf)
  if (tf.s) {
    div.style.transform = `scale(${tf.s})`
  } else {
    div.style.transform = `scale(1)`
  }
  //div.style.transformOrigin = `${tf.ox}px ${tf.oy}px`
  div.style.left = `${parseInt(tf.x)}px`
  div.style.top = `${parseInt(tf.y)}px`
}

// a utility to do the same, for the background, for *the illusion of movement*,
// as a note: something is wrongo with this, background doth not zoom at the same rate...
let writeBackgroundTransform = (div, tf) => {
  div.style.backgroundSize = `${tf.s * 100}px ${tf.s * 100}px`
  div.style.backgroundPosition = `${tf.x + 50*(1-tf.s)}px ${tf.y + 50*(1-tf.s)}px`
}

// a uility to read those transforms out of elements,
// herein lays ancient mods code, probably a better implementation exists
let readTransform = (div) => {
  // transform, for scale
  let transform = div.style.transform
  let index = transform.indexOf('scale')
  let left = transform.indexOf('(', index)
  let right = transform.indexOf(')', index)
  let s = parseFloat(transform.slice(left + 1, right))

  // left and right, position
  let x = parseFloat(div.style.left)
  let y = parseFloat(div.style.top)

  return ({
    s: s,
    x: x,
    y: y
  })
}

let readContainerSize = (div) => {
  return {
    x: div.clientWidth,
    y: div.clientHeight
  }
}

let writeContainerSize = (div, size) => {
  div.style.width = size.x + 'px'
  div.style.height = size.y + 'px'
}

// these two are used in the floop, to build arrays of things to push around
let readXY = (div) => {
  return {
    x: parseFloat(div.style.left),
    y: parseFloat(div.style.top)
  }
}

let dragTool = (dragHandler, upHandler) => {
  let onUp = (evt) => {
    if (upHandler) upHandler(evt)
    window.removeEventListener('mousemove', dragHandler)
    window.removeEventListener('mouseup', onUp)
  }
  window.addEventListener('mousemove', dragHandler)
  window.addEventListener('mouseup', onUp)
}

let getRightHandle = (div) => {
  let x = div.clientWidth
  let y = div.offsetTop + div.clientHeight / 2
  return {
    x: x,
    y: y
  }
}

let getLeftHandle = (div) => {
  let x = div.offsetParent.offsetLeft + (div.offsetLeft)
  let y = div.offsetParent.offsetTop + (div.offsetTop + div.clientHeight / 2)
  return {
    x: x,
    y: y
  }
}

let getFloaterHandle = (div) => {
  let x = parseFloat(div.style.left)
  let y = parseFloat(div.style.top) + (div.clientHeight / 2)
  return {
    x: x,
    y: y
  }
}

let redrawBezier = (bz) => {
  var bl = Math.sqrt(Math.pow((bz.x1 - bz.x2), 2) + Math.pow((bz.y1 - bz.y2), 2)) * 0.6
  var ps = 'M ' + bz.x1 + ' ' + bz.y1 + ' C ' + (bz.x1 + bl) + ' ' + bz.y1
  var pe = ' ' + (bz.x2 - bl) + ' ' + bz.y2 + ' ' + bz.x2 + ' ' + bz.y2
  bz.path.setAttribute('d', ps + pe)
}

// pretty raw drawing routine, will use also for redrawing ...
// we can use smarts upstream of this to call it only on items we need to redraw
let drawWire = (from, to, width) => {
  // defaults
  if (!width) width = 5
  // caseholder for ah new bezier
  let isNew = false
  let bz = {}
  // do we have an array of beziers already?
  if (from.wires) {
    let wi = from.wires.findIndex((cand) => { return cand.to === to })
    if (wi !== -1) {
      bz = from.wires[wi].bz
      // the redraw case: this wire exists, just update its parameters
    } else {
      // make a new one:
      isNew = true
    }
  } else {
    // new wire, new wires array
    from.wires = []
    isNew = true
  }
  if (isNew) {
    // the aux / not-updated work,
    bz.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    $(bz.svg).addClass('svg').addClass('wire')
    bz.path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    bz.svg.style.position = 'absolute'
    bz.svg.style.left = 0 + 'px'
    bz.svg.style.top = 0 + 'px'
    bz.svg.style.overflow = 'visible'
    bz.svg.setAttribute('width', '10')
    bz.svg.setAttribute('height', '10')
    bz.svg.appendChild(bz.path)
    bz.path.addEventListener('mouseenter', (evt) =>{
      bz.path.style.stroke = 'blue'
    })
    bz.path.addEventListener('mouseout', (evt) => {
      bz.path.style.stroke = '#1a1a1a'
    })
    bz.path.addEventListener('contextmenu', (evt) => {
      evt.preventDefault()
      evt.stopPropagation()
      // git the view-tool
      let context = from.handle.parent.context
      context.requestRemoveLink(from.handle, to.handle).then(()=>{
        // redraw? rm self? should happen auto
      }).catch((err) => {
        console.error("could not remove this link")
      })
    })
  }
  // regardless of from whence it came, update it:
  bz.path.style.stroke = '#1a1a1a'
  bz.path.style.fill = 'none'
  bz.path.style.strokeWidth = width + 'px'
  // from, to are blocks, inputs / outputs are blocks ...
  let opp = readTransform(from)
  let ipp = readTransform(to)
  bz.x1 = opp.x + from.clientWidth
  bz.y1 = opp.y + from.clientHeight / 2
  bz.x2 = ipp.x
  bz.y2 = ipp.y + to.clientHeight / 2
  // this runs the maths -> svg string space
  redrawBezier(bz)
  // now, regardless of that:
  if (isNew) {
    from.wires.push({
      to: to,
      bz: bz
    })
  }
  $(from).parent().append(bz.svg)
}

let rmWire = (from, to) => {
  if(!from || !to) return 
  if(!from.wires){
    //console.warn('bad link removal, passing')
    return
  }
  let ind = from.wires.findIndex((cand) => {
    return cand.to == to
  })
  if(ind >= 0){
    $(from.wires[ind].bz.svg).remove()
    from.wires.splice(ind, 1)
  } else {
    //console.warn('bad link at removal, passing')
  }
}

let rmAllWires = (block) => {
  if(!block.wires || (!(block.handle.is == 'input' || block.handle.is == 'output'))){
    return
  }
  for(let wire of block.wires){
    $(wire.bz.svg).remove()
  }
  delete block.wires
}

// also only using this for the view atm,
let addResizeHandle = (elem, callback) => {
  let grab = $(`<div>`).addClass('resizehandle').get(0)
  grab.handle = elem
  $(elem).append(grab)
  writeTransform(grab, {
    x: elem.clientWidth - grab.clientWidth / 2,
    y: elem.clientHeight - grab.clientHeight / 2
  })
}

export default {
  writeTransform,
  writeBackgroundTransform,
  readTransform,
  readContainerSize,
  writeContainerSize,
  dragTool,
  drawWire,
  rmWire,
  rmAllWires,
  addResizeHandle
}
