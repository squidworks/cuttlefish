/*
bootstrap.js

client-side bootup of dataflow environment, and scope thereof

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and cuttlefish projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

'use strict'

// one manager, one scope:
import Manager from './core/manager.js'
import Scope from './core/scope.js'
import blocks from './view/blocks.js'

// our invisible overlord
let nrol = new Manager()
nrol.name = 'cuttlefish'
// init sets type and ind,
nrol.init()

// our toplevel view,
let tlv
let scope

// runs the loop inside of V8's loop: this way our interpreter is non-blocking
// for other js async business
function bootloop() {
  // js u wyldin ! (this is probably slow)
  try {
    nrol.loop()
    // this *is* definitely slow, we should probably run it on a second timer, but that can come
    if (scope) scope.check()
  } catch (err) {
    console.error('ERR @ top of loop:', err)
    console.error('loop halting, mgr bailing')
    return
  }
  if(window.run) setTimeout(bootloop)
}

window.run = true

window.halt = (err) => {
  window.run = false
  console.error('HALTING')
  throw new Error(err)
}

window.onload = () => {
  console.log('BOOTUP')
  nrol.addHunk('view', 'tlview').then((vw) => {
    tlv = vw
    // manager wants view handle to render hunk DOM elements into plane ...
    // there's only one of each, so just
    window.tlv = tlv
    window.nrol = nrol
  }).then(() => {
    // make ah graph,
    // outHunkIndex, outIndex, inHunkIndex, inIndex, debug
    nrol.addLink(1, 0, 0, 0)
    nrol.addLink(0, 0, 1, 0)
    // starts up: view can now pass msgs to the manager,
    bootloop()
    // kick this later
    setTimeout(() => {
      console.log('VW start refresh')
      tlv.refresh().then(() => {
        console.log("VW completes refresh")
        // now we're ready to rumble:
        scope = new Scope($("#wrapper").get(0), tlv)
        window.check = true 
        // placement isn't here *just yet*
        // but should read these in when it does
        tlv.defs[0].position = { x: 250, y: 25 }
        tlv.defs[1].position = { x: 250, y: 70 }
        console.log("Scope created, positions set")
      })
    }, 100)
  }).catch((err) => {
    console.log(err)
  })
}
